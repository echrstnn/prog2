package prog2

import scalafx.Includes._
import scalafx.application._
import scalafx.scene._
import scalafx.scene.layout._
import scalafx.scene.paint.Color

import scalafx.geometry.Insets
import scala.collection.mutable.TreeMap
import scala.collection.mutable.HashMap

object InterfaceView extends Enumeration {
    val Fight, MonsterSelection, StrategySelector, IntroScreen, World, Shop,
        MainMenu = Value
}

object Interface extends Displayable[Scene] {

    /** Le plateau qui est affiché à l'écran
      */
    var board = new NiceBoard(new Board {
        monsters = HashMap((0, 0) -> new Monster)
    })

    /** Représente le panneau de contrôles affiché à droite de l'écran lors d'un
      * combat
      */
    var controls = Controls

    var monsterSelector = new MonsterSelector

    var introScreen = IntroScreen

    var view = InterfaceView.MainMenu
    var nextView = InterfaceView.MainMenu

    var world = World

    var mediaPlayer = Audio.mediaPlayer(2)

    var mediaInd = 0

    var mediaOn = true

    def changeMusic(_ind: Int): Unit = {
        if (!mediaOn) {
            mediaPlayer.stop()
        } else if (mediaInd != _ind) {
            var ind = _ind
            if (_ind == 0) ind = mediaInd
            mediaPlayer.stop()
            mediaPlayer = Audio.mediaPlayer(ind)
            mediaInd = ind
            mediaPlayer.play()
        }
    }

    def scheduleUpdate(): Unit = Platform.runLater(new Runnable {
        override def run(): Unit = Interface.display()
    })

    override protected def displayInit(): Scene = {
        new Scene {
            fill = Color.Beige
            onKeyReleased = e => KeyControls.onKeyReleased(e.code)
            onKeyPressed = e =>
                if (view == InterfaceView.World)
                    KeyControls.onKeyPressed(e.code, e.isShiftDown())
        }
    }

    override protected def displayUpdate(displayObj: Scene): Unit = {
        view match {
            case InterfaceView.MainMenu =>
                changeMusic(3)
                nextView = view
                displayObj.root = MainMenu.display()
            case InterfaceView.Fight =>
                changeMusic(2)
                displayObj.root = new BorderPane() {
                    left = board.display()
                    center = controls.display()
                    padding = Insets(10)
                }
            case InterfaceView.MonsterSelection =>
                displayObj.root = monsterSelector.display()
            case InterfaceView.IntroScreen =>
                displayObj.root = introScreen.display()
            case InterfaceView.World =>
                changeMusic(1)
                nextView = view
                displayObj.root = world.display()
                displayObj.getRoot().requestFocus()
            case InterfaceView.StrategySelector =>
                displayObj.root = StrategySelector.display()
            case InterfaceView.Shop =>
                displayObj.root = Shop.display()
        }
    }
}
