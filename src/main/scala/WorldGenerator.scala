package prog2

import scala.collection.mutable.TreeMap
import scala.util.Random
import scala.collection.mutable.TreeSet

object WorldGenerator {

    var size: Int = 39
    val nbMaps = 5

    private var parent: TreeMap[WorldPosition, WorldPosition] =
        TreeMap()(WorldPositionOrder)

    private def find(a: WorldPosition): WorldPosition = {
        if (parent.get(a).get != a)
            parent.put(a, find(parent.get(a).get))
        parent.get(a).get
    }

    private def union(a: WorldPosition, b: WorldPosition): Boolean = {
        val pa = find(a)
        val pb = find(b)
        if (pa != pb) {
            parent.put(pa, pb)
            true
        } else false
    }

    def generate() = {
        val weight_walls = 1000 * 1000
        val weight_tp = 100 * weight_walls
        var v =
            (for (
              m <- 0 until nbMaps; i <- 1.until(size - 1, 2);
              j <- 1.until(size - 1, 2)
            )
                yield WorldPosition(m, Position(i, j))).toList
        var eh =
            (for (
              m <- 0 until nbMaps; i <- 2.until(size - 1, 2);
              j <- 1.until(size - 1, 2)
            )
                yield (
                  Random.nextInt(weight_walls),
                  WorldPosition(m, Position(i, j)),
                  WorldPosition(m, Position(i - 1, j)),
                  WorldPosition(m, Position(i + 1, j))
                )).toList
        var ev =
            (for (
              m <- 0 until nbMaps; i <- 1.until(size - 1, 2);
              j <- 2.until(size - 1, 2)
            )
                yield (
                  Random.nextInt(weight_walls),
                  WorldPosition(m, Position(i, j)),
                  WorldPosition(m, Position(i, j - 1)),
                  WorldPosition(m, Position(i, j + 1))
                )).toList
        var em =
            (for (
              m <- 1 until nbMaps;
              i <- 1.until(size - 1, 2); j <- 1.until(size - 1, 2)
              if ((i / 2 + j / 2 + m) % 4 <= 1 && m - (i / 2 + j / 2 + m) % 4 - 1 >= 0)
            )
                yield (
                  Random.nextInt(weight_tp),
                  WorldPosition(-1, Position(0, 0)),
                  WorldPosition(
                    m - (i / 2 + j / 2 + m) % 4 - 1,
                    Position(i, j)
                  ),
                  WorldPosition(m, Position(i, j))
                )).toList
        var e = (eh ::: ev ::: em)
            .sortBy(_._1)
            .map({ case (_, ei, v1, v2) => (ei, v1, v2) })
        parent = TreeMap()(WorldPositionOrder)
        v.foreach(pos => parent.put(pos, pos))
        val e_maze =
            for (
              (ei, v1, v2) <- e if (union(v1, v2) ||
                  (ei.map != -1 && Random.nextInt(
                    World.maps(v2.map).antispongeness
                  ) == 0)
                  || (ei.map == -1 && Random.nextInt(
                    weight_tp / weight_walls
                  ) == 0))
            )
                yield (ei, v1, v2)

        var walls: TreeSet[WorldPosition] = TreeSet()(WorldPositionOrder)
        for (m <- 0 until nbMaps; i <- 0 until size; j <- 0 until size)
            walls.add(WorldPosition(m, Position(i, j)))
        e_maze.map(_._1).foreach(walls.remove(_))
        e_maze.map(_._2).foreach(walls.remove(_))
        e_maze.map(_._3).foreach(walls.remove(_))
        walls
            .filter((wp: WorldPosition) =>
                walls.contains(
                  WorldPosition(wp.map, Position(wp.pos.x + 1, wp.pos.y))
                ) || walls.contains(
                  WorldPosition(wp.map, Position(wp.pos.x - 1, wp.pos.y))
                ) || walls.contains(
                  WorldPosition(wp.map, Position(wp.pos.x, wp.pos.y + 1))
                ) || walls.contains(
                  WorldPosition(wp.map, Position(wp.pos.x, wp.pos.y - 1))
                )
            )
            .foreach((wp: WorldPosition) => World.maps(wp.map).addWall(wp.pos))
        e_maze.foreach({ case (wp, v1, v2) =>
            if (wp.map == -1) {
                World.maps(v1.map).addTeleporter(v1.pos, v2)
                World.maps(v2.map).addTeleporter(v2.pos, v1)
            }
        })
        parent = TreeMap()(WorldPositionOrder)
    }
}
