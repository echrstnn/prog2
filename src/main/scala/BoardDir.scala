package prog2

object BoardDir extends Enumeration {

    /** Les 8 directions possibles
      */
    val N, NE, E, SE, S, SW, W, NW = Value

    /** Converti une direction un delta de position
      *
      * @param dir
      *   la direction à convertir
      * @return
      *   un couple d'entiers qui représente le décalage selon x et selon y
      */
    def toPos(dir: BoardDir.Value): (Int, Int) = dir match {
        case N  => (0, -1)
        case NE => (1, -1)
        case E  => (1, 0)
        case SE => (1, 1)
        case S  => (0, 1)
        case SW => (-1, 1)
        case W  => (-1, 0)
        case NW => (-1, -1)
    }

    /** Teste si une direction correspond à un déplacement en diagonale
      *
      * @param dir
      *   la drection à tester
      * @return
      *   true si la direction est parmis NE, SE, SW, NW, false sinon
      */
    def isDiagonal(dir: BoardDir.Value): Boolean =
        List(NE, SE, SW, NW).contains(dir)
}
