package prog2

import scalafx.scene.paint.Color._
import scalafx.scene.layout._
import scalafx.scene.text._
import scalafx.scene.image._
import scalafx.geometry._
import javafx.event.EventHandler
import scalafx.beans.property.ObjectProperty
import scalafx.scene.input._
import java.io.ObjectOutput
import java.io.ObjectInput
import java.io.ObjectOutputStream
import java.io.ObjectInputStream

object MonsterType extends Enumeration {

    /** Les 5 types de monstres
      */
    val Normal, Paper, Rock, Scissors, Well = Value

    /** Calcule si un type est faible face à un autre
      *
      * @param a
      *   un type
      * @param b
      *   un autre type
      * @return
      *   true si a est faible face à b, false sinon
      */
    def isWeak(a: MonsterType.Value, b: MonsterType.Value): Boolean =
        (a, b) match {
            case (Paper, Scissors) => true
            case (Rock, Paper)     => true
            case (Scissors, Rock)  => true
            case (Rock, Well)      => true
            case (Well, Paper)     => true
            case (Scissors, Well)  => true
            case _                 => false
        }

    /** Calcule si un type est fort face à un autre
      *
      * @param a
      *   un type
      * @param b
      *   un autre type
      * @return
      *   true si a est fort face à b, false sinon
      */
    def isStrong(a: MonsterType.Value, b: MonsterType.Value): Boolean =
        isWeak(b, a)
}

class Monster
    extends Displayable[StackPane]
    with Copyable[Monster]
    with Serializable {

    /** HP: Health Points, PP: Power Points
      */
    var (hp, pp) = (16, 6)

    /** indique si le monstre est sous l'effect d'un bouclier */

    var underShield = false

    /** false si le monstre est dans le camp du joueur
      */
    var enemy: Boolean = false

    /** Le type du monstre parmis Normal, Paper, Rock, Scissors
      */
    var monsterType: MonsterType.Value = MonsterType.Normal

    /** Coût en PP d'un déplacement "droit" (ppStraightMove), et en diagonale
      * (ppDiagonalMove)
      */
    var (ppStraightMove, ppDiagonalMove) = (2, 3)

    /** PP gagnés à chaque changement de tour
      */
    var ppByTurn = 3
    var hpByTurn = 0

    /** Listes de attaques disponnibles pour ce monstre
      */
    var attacks: List[Attack] = List()

    /** Image du monstre
      */
    var img = Graphisms.rip

    /** Averti le monstre d'un changement de tour. Lui ajoute ppByTurn PP.
      */
    def newTurn(): Unit = {
        pp = Math.min(99, pp + ppByTurn)
        hp = Math.min(99, hp + hpByTurn)
    }

    /** Retire les PP liés à un déplacement dans une direction donnée. N'a aucun
      * effet si les PP sont insuffisants.
      *
      * @param dir
      *   la direction du déplacement
      * @return
      *   true si les PP ont bien pu être retirés, false si le monstre n'a pas
      *   assez de PP
      */
    def move(dir: BoardDir.Value): Boolean = {
        val ppCost =
            if (BoardDir.isDiagonal(dir)) ppDiagonalMove else ppStraightMove
        if (pp < ppCost)
            return false
        pp -= ppCost
        return true
    }

    /** Retire les PP liés à une attaque. N'a aucun effet si les PP sont
      * insuffisants.
      *
      * @param attack
      *   l'attaque effectuée
      * @param sel
      *   la sélection choisie
      * @return
      *   true les PP ont bien pu être retirés, false si le monstre n'a pas
      *   assez de PP
      */
    def attack(attack: Attack, sel: AttackSelection): Boolean = {
        if (pp < attack.ppCost)
            return false
        pp -= attack.ppCost
        return true
    }

    /** Infliger des dégats à un monstre.
      *
      * @param attack
      *   l'attaque qui inflige des dégats au monstre
      * @return
      *   les HP perdus
      */
    def receiveDamage(attack: Attack): Int = {
        var hpDamage = attack.hpDamage
        if (underShield && hpDamage > 0) {
            return 0
        }
        if (underShield && attack.ppDamage > 0)
            return 0
        if (MonsterType.isWeak(monsterType, attack.attackType))
            hpDamage *= 2
        if (MonsterType.isStrong(monsterType, attack.attackType))
            hpDamage /= 2
        hp -= hpDamage
        pp -= attack.ppDamage
        if (attack.shield)
            underShield = true
        hpDamage
    }

    override def copy(): Monster = new Monster copyFrom this

    override def copyFrom(o: Monster): Monster = {
        hp = o.hp
        pp = o.pp
        enemy = o.enemy
        underShield = o.underShield
        monsterType = o.monsterType
        ppStraightMove = o.ppStraightMove
        ppDiagonalMove = o.ppDiagonalMove
        ppByTurn = o.ppByTurn
        hpByTurn = o.hpByTurn
        attacks = o.attacks //.map(_.copy())
        img = o.img
        return this
    }

    def equals(o: Monster): Boolean = {
        hp == o.hp &&
        pp == o.pp &&
        enemy == o.enemy &&
        underShield == o.underShield &&
        monsterType == o.monsterType &&
        ppStraightMove == o.ppStraightMove &&
        ppDiagonalMove == o.ppDiagonalMove &&
        ppByTurn == o.ppByTurn &&
        hpByTurn == o.hpByTurn &&
        attacks == o.attacks
    }

    override def hashCode(): Int = {
        var ans = if (enemy) 1 else 0
        ans = ans * 41 + hp
        ans = ans * 41 + pp
        ans = ans * 7 + ppStraightMove
        ans = ans * 7 + ppByTurn
        ans = ans * 2 + hpByTurn
        ans
    }

    private def writeObject(oos: ObjectOutputStream) = {
        oos.writeInt(hp)
        oos.writeInt(pp)
        oos.writeBoolean(enemy)
        oos.writeBoolean(underShield)
        oos.writeObject(monsterType)
        oos.writeInt(ppStraightMove)
        oos.writeInt(ppDiagonalMove)
        oos.writeInt(ppByTurn)
        oos.writeInt(hpByTurn)
        oos.writeObject(attacks)
        oos.writeObject(img.getUrl())
    }

    private def readObject(ois: ObjectInputStream) = {
        hp = ois.readInt()
        pp = ois.readInt()
        enemy = ois.readBoolean()
        underShield = ois.readBoolean()
        monsterType = ois.readObject().asInstanceOf[MonsterType.Value]
        ppStraightMove = ois.readInt()
        ppDiagonalMove = ois.readInt()
        ppByTurn = ois.readInt()
        hpByTurn = ois.readInt()
        attacks = ois.readObject().asInstanceOf[List[Attack]]
        img = new Image(ois.readObject().asInstanceOf[String])
    }

    override protected def displayInit(): StackPane = {
        val thisMonster = this
        new StackPane {
            filterEvent(MouseEvent.Any) { () =>
                {
                    Controls.monsterInfo.children = List(
                      new MonsterSelector()
                          .displayMonsterEntry(
                            thisMonster,
                            if (enemy) Red else Green
                          )
                    )
                }
            }
        }
    }

    override protected def displayUpdate(displayObj: StackPane): Unit = {
        val o = new ImageView(img) {
            fitHeight = 64; fitWidth = 64
        }
        val c = if (enemy) DarkRed else DarkGreen
        val hpbt = if(hpByTurn != 0) s"  $hpByTurn" else ""
        val o2 = new Text(s"$hp HP$hpbt") {
            alignmentInParent = Pos.TopLeft; fill = c;
            style = "-fx-font: normal bold 10pt sans-serif"
        }
        val o3 = new Text(s"$pp PP") {
            alignmentInParent = Pos.BottomLeft
            style = "-fx-font: normal bold 10pt sans-serif"
        }
        val o4 = new ImageView(Graphisms.shield) {
            fitHeight = 20; fitWidth = 20
            alignmentInParent = Pos.BottomRight
        }
        displayObj.children = List(o, o2, o3)
        if (underShield)
            displayObj.children += o4
    }
}

object Monster {
    val menhir = new Monster {
        img = Graphisms.menhir
        hp = 9
        ppByTurn = 4
        ppStraightMove = 5; ppDiagonalMove = 7
        monsterType = MonsterType.Rock
        attacks = List(Attack.rock_punch, Attack.rolling_stones)
    }
    val milo = new Monster {
        img = Graphisms.milo
        hp = 14
        ppByTurn = 5
        ppStraightMove = 3; ppDiagonalMove = 4
        monsterType = MonsterType.Rock
        attacks = List(Attack.rock_punch, Attack.healing_power)
    }
    val moai = new Monster {
        img = Graphisms.moai
        hp = 21
        ppByTurn = 4
        ppStraightMove = 5; ppDiagonalMove = 7
        monsterType = MonsterType.Rock
        attacks = List(Attack.throw_pebble, Attack.falling_moais, Attack.shield)
    }
    val wall = new Monster {
        img = Graphisms.wall
        hp = 27
        ppByTurn = 3
        ppStraightMove = 5; ppDiagonalMove = 7
        monsterType = MonsterType.Rock
        attacks = List(Attack.throw_pebble, Attack.wall, Attack.shield)
    }
    val sphinx = new Monster {
        img = Graphisms.sphinx
        hp = 33
        ppByTurn = 4
        ppStraightMove = 6; ppDiagonalMove = 8
        monsterType = MonsterType.Rock
        attacks =
            List(Attack.earthquake, Attack.sphinx_power, Attack.healing_power)
    }
    val sheet = new Monster {
        img = Graphisms.sheet
        hp = 5
        ppByTurn = 5
        ppStraightMove = 3; ppDiagonalMove = 4
        monsterType = MonsterType.Paper
        attacks = List(Attack.paper_punch, Attack.ink_splash)
    }
    val letter = new Monster {
        img = Graphisms.letter
        hp = 7
        ppByTurn = 6
        ppStraightMove = 3; ppDiagonalMove = 4
        monsterType = MonsterType.Paper
        attacks = List(Attack.basic_punch, Attack.usps_truck)
    }
    val printer = new Monster {
        img = Graphisms.printer
        hp = 9
        ppByTurn = 3
        ppStraightMove = 3; ppDiagonalMove = 4
        monsterType = MonsterType.Paper
        attacks = List(Attack.paper_punch, Attack.print)
    }
    val swan = new Monster {
        img = Graphisms.swan
        hp = 12
        ppByTurn = 5
        ppStraightMove = 2; ppDiagonalMove = 3
        monsterType = MonsterType.Paper
        attacks = List(Attack.ink_splash, Attack.origami, Attack.ppboost)
    }
    val crane = new Monster {
        img = Graphisms.crane
        hp = 18
        ppByTurn = 6
        ppStraightMove = 2; ppDiagonalMove = 3
        monsterType = MonsterType.Paper
        attacks = List(Attack.ink_splash, Attack.origami, Attack.switch)
    }
    val ciseaux = new Monster {
        img = Graphisms.ciseaux
        hp = 7
        ppByTurn = 5
        ppStraightMove = 5; ppDiagonalMove = 7
        monsterType = MonsterType.Scissors
        attacks = List(Attack.scissors_punch, Attack.cutcutcut)
    }
    val ciseau = new Monster {
        img = Graphisms.ciseau
        hp = 11
        ppByTurn = 4
        ppStraightMove = 3; ppDiagonalMove = 4
        monsterType = MonsterType.Scissors
        attacks = List(Attack.scissors_punch, Attack.carve, Attack.weakness)
    }
    val couteau = new Monster {
        img = Graphisms.couteau
        hp = 16
        ppByTurn = 5
        ppStraightMove = 3; ppDiagonalMove = 4
        monsterType = MonsterType.Scissors
        attacks = List(Attack.stab, Attack.throw_knife, Attack.weakness)
    }
    val katana = new Monster {
        img = Graphisms.katana
        hp = 22
        ppByTurn = 5
        ppStraightMove = 3; ppDiagonalMove = 4
        monsterType = MonsterType.Scissors
        attacks = List(Attack.cutcutcut, Attack.katana_strike, Attack.switch)
    }
    val invader = new Monster {
        img = Graphisms.invader
        hp = 24
        ppByTurn = 4
        ppStraightMove = 2; ppDiagonalMove = 3
        monsterType = MonsterType.Scissors
        attacks = List(Attack.stab, Attack.invaders)
    }
    val puits = new Monster {
        img = Graphisms.puits
        hp = 35
        ppByTurn = 3
        ppStraightMove = 6; ppDiagonalMove = 8
        monsterType = MonsterType.Well
        attacks = List(Attack.hole, Attack.healing_power)
    }
    val volcan = new Monster {
        img = Graphisms.volcan
        hp = 75
        ppByTurn = 3
        ppStraightMove = 6; ppDiagonalMove = 8
        monsterType = MonsterType.Well
        attacks = List(Attack.lava, Attack.eruption, Attack.shield)
    }

    val allL1 = List(menhir, sheet, ciseaux)
    val allL2 = List(letter, ciseau, milo, printer)
    val allL3 = List(moai, swan, couteau, wall)
    val allL4 = List(sphinx, crane, katana, invader)
    val allSpec = List(puits, volcan)

    val allMonster = allL1 ::: allL2 ::: allL3 ::: allL4 ::: allSpec

    val allMonsterx2 = allMonster.flatMap((m: Monster) => {
        List(m, m.copy())
    })
}
