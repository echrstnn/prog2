package prog2
import javafx.scene
import scalafx.Includes._
import scalafx.scene.layout._
import scalafx.scene.image._
import scalafx.scene.text._
import scalafx.geometry._
import scalafx.scene.control._
import scalafx.application._
import scalafx.scene.input._
import scalafx.scene.control.ScrollPane._

import scala.collection.mutable
import scala.util.Random
import scala.collection.immutable.HashMap
import scalafx.scene.paint.LinearGradient
import scalafx.scene.paint.Stops
import scalafx.scene.paint.Color

class WorldMap(mapId: Int) extends Displayable[BorderPane] {

    var size = WorldGenerator.size
    var antispongeness = Array(5, 2, 1, 8, 15)(mapId)

    var mapName =
        Array("Forêt paisible", "Bureau", "Lune", "Bois hanté", "Lac").apply(
          mapId
        )

    var usedPositions = new mutable.TreeSet[Position]()(PositionOrder)
    if (mapId == 0) {
        usedPositions.add(Position(size / 2, size / 2))
        usedPositions.add(Position(size / 2 + 1, size / 2))
        usedPositions.add(Position(size / 2, size / 2 + 1))
        usedPositions.add(Position(size / 2 + 1, size / 2 + 1))
    }

    var worldGp: GridPane = new GridPane {
        columnConstraints =
            for (_ <- 0 until size) yield new ColumnConstraints(48) {
                halignment = HPos.Center
            }
        rowConstraints = for (_ <- 0 until size) yield new RowConstraints(48) {
            valignment = VPos.Center
        }
    }

    var nbMonsters = 0
    var textMapDesc = new Text {
        style = "-fx-font: normal bold 12pt sans-serif"
        fill = new LinearGradient(
          endX = 0,
          stops = Stops(Color.Purple, Color.Black)
        )
    }

    def updateDesc() = textMapDesc.text = s"$mapName\n$nbMonsters monstres"

    var imgPlayerMoving =
        HashMap(
          KeyCode.DOWN -> "d",
          KeyCode.UP -> "u",
          KeyCode.RIGHT -> "r",
          KeyCode.LEFT -> "l"
        ).mapValues((s: String) =>
            new Image(s"file:src/main/resources/char_$mapId$s.webp.gif")
        )

    var imgPlayer =
        HashMap(
          KeyCode.DOWN -> "d",
          KeyCode.UP -> "u",
          KeyCode.RIGHT -> "r",
          KeyCode.LEFT -> "l"
        ).mapValues((s: String) =>
            new Image(s"file:src/main/resources/char_$mapId$s.webp.png")
        )

    var player: ImageView = new ImageView(imgPlayer.get(KeyCode.DOWN).get) {
        layoutX = 900
        layoutY = 900
        fitHeight = 75
        fitWidth = 75
        alignmentInParent = Pos.TopLeft
    }

    var worldPane: Pane = new Pane {
        alignmentInParent = Pos.Center
        children = List(worldGp, player)
        if (mapId == 2)
            children = new ImageView(Graphisms.moon) {
                scaleX = 1.25; scaleY = 1.25
            } :: List(worldGp, player)
    }

    var scrollPane: ScrollPane = new ScrollPane {
        val szh = 700
        val szw = 1200
        prefHeight = szh
        maxHeight = szh
        prefWidth = szw
        maxWidth = szw
        vvalue = 0.5
        hvalue = 0.5
        alignmentInParent = Pos.Center
        content = worldPane
        hbarPolicy = ScrollBarPolicy.Never
        vbarPolicy = ScrollBarPolicy.Never

        filterEvent(
          ScrollEvent.Scroll
        )(FilterMagnet.fromEvent[javafx.scene.input.ScrollEvent, ScrollEvent]({
            e: ScrollEvent => e.consume()
        })({ e: javafx.scene.input.ScrollEvent =>
            new ScrollEvent(e)
        }))
    }

    var dialogueText = Dialogues.textObj()

    def randomPosition(): Position = {
        var i = 1
        var j = 1
        while (usedPositions.contains(Position(i, j))) {
            i = 2 * Random.nextInt(size / 2) + 1
            j = 2 * Random.nextInt(size / 2) + 1
        }
        Position(i, j)
    }

    def addMonster(
        m: Monster,
        selLimit: Int,
        nbCompagnons: Int,
        compagnons: List[Monster]
    ): Unit = {
        val call = (wm: WorldMap) =>
            wm.addMonster(m, selLimit, nbCompagnons, compagnons)
        SaveFile.registerCall(mapId, call)
        nbMonsters += 1
        updateDesc()
        val pos = randomPosition()
        val img = new ImageView(m.img) {
            fitWidth = 48
            fitHeight = 48
        }
        val blocker = new ImageView(Graphisms.shield) {
            fitWidth = 144
            fitHeight = 144
        }
        blocker.opacity = 0
        blocker.userData = () => true
        img.userData = () => {

            if (Dialogues.displayed)
                true
            else {
                // worldGp.add(
                //   blocker,
                //   pos.x,
                //   pos.y
                // )

                Dialogues
                    .fromList(List("À l'attaque !"))
                    .displayDialogue(() => {
                        if (Interface.view == InterfaceView.World) {
                            Interface.view = InterfaceView.MonsterSelection
                            Platform runLater {
                                Interface.monsterSelector =
                                    new MonsterSelector {
                                        selectionLimit = selLimit
                                        availableMonsters =
                                            Player.monsters.toList
                                        enemyMonsters = m :: Random
                                            .shuffle(compagnons)
                                            .slice(0, nbCompagnons)
                                        successFun = () => {
                                            SaveFile.unregisterCall(mapId, call)
                                            nbMonsters -= 1
                                            updateDesc()
                                            worldGp.children.remove(img)
                                            val idx = Player.monsters.indexOf(m)
                                            if (idx == -1)
                                                Player.monsters += m
                                            else
                                                Player.monsters
                                                    .insert(idx, m.copy())
                                        }
                                    }
                                worldGp.children.remove(blocker)
                                Interface.scheduleUpdate()
                            }
                        }
                        true
                    })
            }
        }
        usedPositions += pos
        worldGp.add(
          img,
          pos.x,
          pos.y
        )
    }

    def addChest(chest: ChestsAward): Unit = {
        val call = (wm: WorldMap) => wm.addChest(chest)
        SaveFile.registerCall(mapId, call)
        val img = new ImageView(chest.icon) {
            fitWidth = 48
            fitHeight = 48
        }
        var obtained = false
        img.userData = () => {
            if (!obtained) {
                chest.award()
                obtained = true
                img.opacity = 0
                SaveFile.unregisterCall(mapId, call)
            }
            false
        }
        val pos = randomPosition()
        usedPositions += pos
        worldGp.add(
          img,
          pos.x,
          pos.y
        )
    }

    def notFreeSquare(pos: Position, n: Int): Boolean = {
        var used = false
        for (k <- (-n) to n; l <- (-n) to n) {
            used =
                used || usedPositions.contains(Position(pos.x + k, pos.x + l))
        }
        used
    }

    def addShop(): Unit = {
        var i = 1
        var j = 1
        while (
          notFreeSquare(
            Position(i, j),
            1
          ) || i == 1 || j == 1 || i == size - 2 || j == size - 2
        ) {
            i = 2 * Random.nextInt(size / 2) + 1
            j = 2 * Random.nextInt(size / 2) + 1
        }
        addShop_(i, j)
    }

    def addShop_(i: Int, j: Int): Unit = {
        SaveFile.registerCall(mapId, _.addShop_(i, j))
        val img = new ImageView(Graphisms.shop) {
            fitWidth = 48 * 3
            fitHeight = 48 * 3
        }
        val blocker = new ImageView(Graphisms.shield) {
            fitWidth = 240
            fitHeight = 240
        }
        blocker.opacity = 0
        blocker.userData = () => true
        img.userData = () => {
            if (Dialogues.displayed)
                true
            else {
                // worldGp.add(
                //   blocker,
                //   i,
                //   j
                // )

                Dialogues
                    .fromList(List("Bienvenue dans mon magasin !"))
                    .displayDialogue(() => {
                        Interface.view = InterfaceView.Shop
                        Interface.scheduleUpdate()
                        worldGp.children.remove(blocker)
                        true
                    })
            }
        }
        for (k <- (-2) to 2; l <- (-2) to 2) {
            usedPositions += Position(i + k, j + l)
        }
        worldGp.add(
          img,
          i,
          j
        )
    }

    def addWall(pos: Position): Unit = {
        if (!usedPositions.contains(pos)) {
            usedPositions += pos
            SaveFile.registerCall(mapId, _.addWall(pos))
            worldGp.add(
              new ImageView(Graphisms.buisson(mapId)) {
                  fitHeight = 48; fitWidth = 48
                  userData = () =>
                      pos.x == 0 || pos.y == 0 || pos.x == size - 1 || pos.y == size - 1 || (!Player.cheat)
              },
              pos.x,
              pos.y
            )
        }
    }

    def addTeleporter(pos: Position, dest: WorldPosition): Unit = {
        SaveFile.registerCall(mapId, _.addTeleporter(pos, dest))
        usedPositions += pos
        worldGp.add(
          new ImageView(Graphisms.teleporter(dest.map)) {
              fitHeight = 36; fitWidth = 36
              userData = () => {
                  Dialogues
                      .fromList(
                        List(s"Aller vers ${World.maps(dest.map).mapName}")
                      )
                      .displayDialogue(() => {
                          World.currMap = World.maps(dest.map)
                          World.currMap.player.layoutX = layoutX.value - 20
                          World.currMap.player.layoutY = layoutY.value - 30
                          World.currMap.scrollPane.hvalue = scrollPane.hvalue()
                          World.currMap.scrollPane.vvalue = scrollPane.vvalue()
                          Interface.display()
                          true
                      })
                  false
              }
          },
          pos.x,
          pos.y
        )
    }

    def focusOnPlayer(): Unit = {
        val pcx = player.layoutX() + player.fitWidth() / 2
        val pcy = player.layoutY() + player.fitHeight() / 2
        scrollPane.hvalue = (pcx - scrollPane.width() / 2) / (worldPane
            .width() - scrollPane.width())
        scrollPane.vvalue = (pcy - scrollPane.height() / 2) / (worldPane
            .height() - scrollPane.height())
    }

    for (i <- 0 until size; j <- 0 until size) {
        worldGp.add(
          new ImageView(Graphisms.terre(mapId)) {
              fitHeight = 48; fitWidth = 48
              userData = () => false
          },
          i,
          j
        )
    }

    override protected def displayInit(): BorderPane = {
        new BorderPane {
            alignmentInParent = Pos.Center
            center = new StackPane {
                children = List(
                  scrollPane,
                  new BorderPane {
                      opacity = 0.75
                      right = new VBox {
                          alignment = Pos.TopCenter
                          children = List(
                            Player.moneyDisplay,
                            ControlButtons.baseButton(
                              None,
                              "Main menu",
                              _ => {
                                  Interface.view = InterfaceView.MainMenu
                                  Interface.scheduleUpdate()
                              }
                            ),
                            ControlButtons.baseButton(
                              None,
                              "Move player",
                              _ => {
                                  Interface.displayObj.getRoot().requestFocus()
                                  focusOnPlayer()
                              }
                            ),
                            ControlButtons.baseButton(
                              None,
                              "Move camera",
                              _ => {
                                  scrollPane.requestFocus()
                              }
                            ),
                            ControlButtons.baseButton(
                              None,
                              "My monsters",
                              _ => {
                                  Interface.view =
                                      InterfaceView.MonsterSelection
                                  Interface.monsterSelector =
                                      new MonsterSelector {
                                          showOnly = true
                                      }
                                  Interface.scheduleUpdate()
                              }
                            )
                          )
                          if (Player.cheat) {
                              children += ControlButtons.baseButton(
                                None,
                                "Cheat",
                                _ => {
                                    Player.cheat = !Player.cheat
                                    if (Player.cheat) {
                                        Player.monsters = mutable.ListBuffer()
                                        Player.monsters.appendAll(
                                          Monster.allMonsterx2
                                        )
                                    }
                                }
                              )
                          }
                          children += Dialogues.baseText(
                            "Use arrows to move.\n Press Shift to move faster",
                            dialogueText
                          )
                          children += textMapDesc
                      }
                  }
                )
            }
            padding = Insets(10)
        }
    }

    override protected def displayUpdate(displayObj: BorderPane): Unit = {
        ()
    }
}
