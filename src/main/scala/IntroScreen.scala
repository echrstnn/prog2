package prog2

import scalafx.scene.layout._
import scalafx.scene.image._
import scalafx.scene.text._
import scalafx.geometry._
import scalafx.scene.paint._
import scalafx.scene.control._

object IntroScreen extends Displayable[BorderPane] {
    val nameAsked = new TextField()
    nameAsked.maxWidth = 150
    nameAsked.alignment = Pos.Center

    def testeur(string:String): Boolean ={
        val s = string.toLowerCase
        s=="louis"|| s=="stefan"|| s=="schwoon"|| s=="lemonnier"|| s=="stefan schwoon"|| s=="louis lemonnier"
    }

    override protected def displayInit(): BorderPane = {
        val startButton = ControlButtons.baseButton(None,"Commencer",_ => {
                Player.name = nameAsked.text()
                Player.cheat = testeur(Player.name)
                Interface.view = InterfaceView.World
                Interface.scheduleUpdate()
            })
        val msg = ControlButtons.baseButton(None,"Entrez votre Nom",()=>_)
        msg.padding = Insets(120,0,20,0)
        new BorderPane {
            alignmentInParent = Pos.Center
            top=msg
            bottom = startButton
            center = nameAsked
            padding = Insets(10)
        }
    }

    override protected def displayUpdate(displayObj: BorderPane): Unit = {
    }
}