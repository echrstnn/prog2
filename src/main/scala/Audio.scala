package prog2

import scalafx.scene.media._
import scalafx.util.Duration

object Audio {

    val theme1 = new Media(getClass().getResource("/theme1.mp3").toURI.toString)
    val theme2 = new Media(getClass().getResource("/theme2.mp3").toURI.toString)
    val theme3 = new Media(getClass().getResource("/theme3.mp3").toURI.toString)

    def mediaPlayer(i: Int) = {
        i match {
            case 0 => new MediaPlayer(theme1) {
                stopTime = new Duration(191000)
                cycleCount = Int.MaxValue
                volume = 0
            }
            case 1 => new MediaPlayer(theme1) {
                stopTime = new Duration(191000)
                cycleCount = Int.MaxValue
                volume = 100
            }
            case 2 => new MediaPlayer(theme2) {
                startTime = new Duration(4000)
                stopTime = new Duration(80880)
                cycleCount = Int.MaxValue
                volume = 100
            }
            case 3 => new MediaPlayer(theme3) {
                startTime = new Duration(7500)
                cycleCount = Int.MaxValue
                volume = 100
            }

        }
    }
}
