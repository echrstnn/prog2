package prog2

case class Position(x:Int,y:Int)

object PositionOrder extends Ordering[Position]{
    override def compare(pos1: Position, pos2: Position) : Int =
        if (pos1.x==pos2.x)
            pos1.y-pos2.y
        else
            pos1.x-pos2.x
}

case class WorldPosition(map:Int, pos:Position)

object WorldPositionOrder extends Ordering[WorldPosition]{
    override def compare(pos1: WorldPosition, pos2: WorldPosition) : Int =
        if (pos1.map == pos2.map)
            PositionOrder.compare(pos1.pos, pos2.pos)
        else
            pos1.map - pos2.map
}