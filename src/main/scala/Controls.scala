package prog2

import scalafx.scene.paint._
import scalafx.scene._
import scalafx.scene.image._
import scalafx.scene.shape._
import scalafx.scene.layout._
import scalafx.scene.control._
import scalafx.scene.text._
import scalafx.scene.image._
import scalafx.scene.paint.Color._
import scalafx.scene.shape._
import scalafx.event.EventHandler
import scalafx.scene.input._
import scalafx.scene.paint.Color._
import scalafx.scene.text.Text

import scala.collection.mutable._
import scalafx.application.Platform
import scalafx.geometry.Pos._
import scalafx.geometry.Insets

import scalafx.geometry.Pos

object Controls extends Displayable[VBox] {

    var posSelected = (false, (0, 0))

    var inputBlocked = false

    var inventorymenu = false

    var monsterInfo = new StackPane {
        alignment = Center
        children = List(
          new Text(
            "Put your mouse over a monster\nto see information about him"
          )
        )
    }

    override protected def displayInit(): VBox = new VBox {
        alignmentInParent = Center
        alignment = Center
    }

    override protected def displayUpdate(displayObj: VBox): Unit = {
        if (inventorymenu)
            displayObj.children =
                ControlButtons.cancelInventory :: Inventory.objectButtons()
        else {
            val nt = ControlButtons.newTurn

            displayObj.children = List(Pad.gp())
            if (Pad.atk)
                displayObj.children += ControlButtons.baseButton(
                  None,
                  "exit",
                  _ => ControlButtons.clicked(_ => Pad.atk = false)
                )
            displayObj.children += nt

            Interface.board.curMonster.attacks.foreach((f: Attack) => {
                displayObj.children += ControlButtons.atkButton(f)
            })
            displayObj.children += ControlButtons.inventoryButton
            displayObj.children += monsterInfo
        }

    }
}

object Pad {

    var atk = false

    var currAtk = Attack.basic_punch

    def padDepl() = {
        val gp = new GridPane
        BoardDir.values.foreach((dir: BoardDir.Value) => {
            gp.add(
              ControlButtons.dirButtonDepl(dir),
              1 + BoardDir.toPos(dir)._1,
              1 + BoardDir.toPos(dir)._2
            )
        })
        val info = new Text {
            text =
                s"Straight move :\n ${Interface.board.curMonster.ppStraightMove} PPs \n\n Diagonal move :\n ${Interface.board.curMonster.ppDiagonalMove} PPs"
            style = "-fx-font: normal bold 12pt sans-serif"
            fill = new LinearGradient(endX = 0, stops = Stops(Purple, Black))
        }
        new HBox {
            children = List(gp, info)
            alignment = Center
        }
    }

    val padAtk = new GridPane {
        alignmentInParent = Pos.Center
    }

    BoardDir.values.foreach((dir: BoardDir.Value) => {
        padAtk.add(
          ControlButtons.dirButtonAtk(dir),
          1 + BoardDir.toPos(dir)._1,
          1 + BoardDir.toPos(dir)._2
        )
    })

    def gp() = {
        if (atk) {
            new HBox {
                children = List(padAtk)
                alignment = Center
            }
        } else {
            padDepl()
        }
    }
}

object ControlButtons {

    def clicked(action: Board => Unit) = {
        new Thread {
            override def run() = {
                if (!Controls.inputBlocked) {
                    Controls.inputBlocked = true
                    Pad.atk = false
                    Interface.board.gpRange.mouseTransparent = true
                    Platform.runLater {
                        Interface.board.gpRange.children.clear()
                        Interface.board.gpRangeMouse.children.clear()
                    }
                    action(Interface.board)
                    Controls.inputBlocked = false
                    Interface.scheduleUpdate()
                }
            }
        }.start()
    }

    /** base de la plupart des bouttons */
    def baseButton(icon: Option[Image], label: String, f: Unit => Unit) = {

        val labButton = new Text {
            x = icon match {
                case None => 38
                case _    => 60
            }
            y = 30
            text = label
            style = "-fx-font: normal bold 15pt sans-serif"
            fill = new LinearGradient(endX = 0, stops = Stops(Black, Gold))
            mouseTransparent = true
        }

        val buttonBase = new ImageView(Graphisms.attackButtonBase) {
            fitWidth = icon match {
                case Some(_) => 90 + 1.86 * labButton.maxWidth(0)
                case None    => 70 + 1.86 * labButton.maxWidth(0)
            }
            fitHeight = 50
            onMouseClicked = _ => (f())
        }

        icon match {
            case None =>
                new StackPane {
                    children = List(buttonBase, labButton)
                    padding = Insets(5, 0, 5, 0)
                    alignmentInParent = Center
                }
            case Some(im) =>
                new Pane {
                    children = List(
                      buttonBase,
                      new ImageView(im) {
                          x = 10
                          y = 5
                          fitWidth = 40
                          fitHeight = 40
                          mouseTransparent = true
                          alignmentInParent = CenterLeft
                      },
                      labButton
                    )
                    padding = Insets(5, 0, 5, 0)
                    alignmentInParent = Center
                }
        }
    }

    /** action a effectuer lorsque le bouton de l'attaque "f" est préssé.
      */
    def actionatk(f: Attack) = (b: Board) => {
        f.selection match {
            case NoSelection() =>
                if (b.curMonster.pp >= f.ppCost) {
                    b.attack(f, NoSelection); ()
                }

            case DirectionSelection(_) =>
                if (!Pad.atk && (b.curMonster.pp >= f.ppCost)) {
                    Pad.atk = true;
                    Pad.currAtk = f;
                    Interface.scheduleUpdate()
                    ()
                }

            case BoardSquareSelection(_) =>
                if (b.curMonster.pp >= f.ppCost) {
                    Interface.board.gpRange.mouseTransparent = false
                    Targets.currAtk = f
                    Targets.target(f.selection_range, b.curMonsterPos)
                    Interface.scheduleUpdate()
                    ()
                }
                Controls.inventorymenu = false
        }
    }

    def atkButton(attack: Attack): Pane = {

        val rangeText =
            if (attack.selection == BoardSquareSelection)
                s"${attack.range} + ${attack.selection_range}"
            else s"${attack.range}"
        val infos = new Text {
            text =
                s"[${attack.attackType}]\nDamage: ${attack.hpDamage}    Cost: ${attack.ppCost}\nRange: $rangeText\n${attack.desc}"
            style = "-fx-font: normal 11pt sans-serif"
            margin = Insets(0, 0, 0, 10)
            fill = new LinearGradient(endX = 0, stops = Stops(Red, Purple))
        }

        val button = baseButton(
          Some(attack.selection match {
              case NoSelection()           => Graphisms.iconCQC
              case DirectionSelection(_)   => Graphisms.iconLRA
              case BoardSquareSelection(_) => Graphisms.iconTA
          }),
          attack.name,
          _ => clicked(actionatk(attack))
        )

        if (attack.ppCost > Interface.board.curMonster.pp)
            button.opacity = 0.5

        new HBox {
            children = List(button, infos)
            padding = Insets(5)
            alignment = CenterLeft
        }
    }

    def dirButtonAtk(dir: BoardDir.Value) = {
        def atkdirselector(b: Board) = {
            Pad.atk = false
            b.attack(Pad.currAtk, DirectionSelection(dir))
            ()
        }

        new ImageView(Graphisms.dirToIm(dir, true)) {
            fitWidth = 50
            fitHeight = 50
            onMouseClicked = _ => clicked(atkdirselector)
        }
    }

    def dirButtonDepl(dir: BoardDir.Value) = {
        val ppCost =
            if (BoardDir.isDiagonal(dir))
                Interface.board.curMonster.ppDiagonalMove
            else Interface.board.curMonster.ppStraightMove
        new ImageView(Graphisms.dirToIm(dir, false)) {
            fitWidth = 50
            fitHeight = 50
            onMouseClicked = _ => clicked(_.move(dir))
            opacity =
                if (ppCost > Interface.board.curMonster.pp) 0.5
                else 1
        }

    }

    val cancelInventory = baseButton(
      None,
      "Exit",
      _ => {
          Controls.inventorymenu = false
          Interface.scheduleUpdate()
      }
    )

    val inventoryButton = baseButton(
      None,
      "Inventory",
      _ => {
          if (!Controls.inputBlocked)
              Controls.inventorymenu = true
          Interface.scheduleUpdate()
      }
    )

    val newTurn =
        baseButton(
          None,
          "End turn",
          _ =>
              clicked((b: Board) => {
                  b.newTurn()
                  if(!b.pvp)
                  while (
                    b.curMonster.enemy && Interface.view == InterfaceView.Fight
                  ) {
                      val move = Bot.botChoice(b)
                      val ans = move(b)
                      if (!ans)
                          println("wrong choice")
                  }
              })
        )

}
