package prog2
import javafx.scene
import scalafx.Includes._
import scalafx.scene.layout._
import scalafx.scene.image._
import scalafx.scene.text._
import scalafx.geometry._
import scalafx.scene.control._
import scalafx.application._
import scalafx.scene.input._
import scalafx.scene.control.ScrollPane._
import javafx.geometry

import scala.collection.mutable
import scala.util.Random

object KeyControls {

    var lastKey = KeyCode.TAB
    val normalSpeed = 4
    val fastSpeed = 15
    var moveSpeed = normalSpeed
    var keyThread = new Thread

    def onKeyReleased(k: KeyCode): Unit = {
        if (k == KeyCode.SHIFT) moveSpeed = normalSpeed
        else if (k == lastKey) {
            keyThread.stop()
            World.currMap.imgPlayer
                .get(lastKey)
                .foreach(World.currMap.player.image = _)
        }
    }

    def getKeyThread(dx_ : Int, dy_ : Int): Thread = {
        new Thread {
            override def run(): Unit = {
                while (moveSpeed > 0) {
                    var moved = true
                    while (moved) {
                        var dx = dx_ * moveSpeed
                        var dy = dy_ * moveSpeed
                        Platform runLater {
                            if (Dialogues.displayed)
                                Dialogues.refresh()
                            World.currMap.player.layoutX.value += dx
                            World.currMap.player.layoutY.value += dy
                            var playerBounds =
                                World.currMap.player.getBoundsInParent()
                            playerBounds = new geometry.BoundingBox(
                              playerBounds.minX + playerBounds.width / 3,
                              playerBounds.minY + playerBounds.height * 2 / 3,
                              playerBounds.width / 3,
                              playerBounds.height / 4
                            )
                            val intersected =
                                World.currMap.worldGp.children.filter(
                                  _.getBoundsInParent() intersects playerBounds
                                )
                            if (
                              intersected.exists((n: scene.Node) =>
                                  n.userData match {
                                      case f: (() => Boolean) => f()
                                  }
                              )
                            ) {
                                World.currMap.player.layoutX.value -= dx
                                World.currMap.player.layoutY.value -= dy
                                moved = false
                            }
                            World.currMap.focusOnPlayer()
                        }

                        Thread.sleep(30)
                    }
                    moveSpeed -= 1
                }
            }
        }
    }

    def onKeyPressed(k: KeyCode, shiftDown: Boolean) {
        if (k == KeyCode.SHIFT)
            moveSpeed = fastSpeed
        else if (lastKey != k || !keyThread.isAlive()) {
            moveSpeed = moveSpeed max normalSpeed
            if (shiftDown) moveSpeed = fastSpeed
            lastKey = k
            keyThread.stop()
            val dx = k match {
                case KeyCode.LEFT  => -1
                case KeyCode.RIGHT => +1
                case _             => 0
            }
            val dy = k match {
                case KeyCode.UP   => -1
                case KeyCode.DOWN => +1
                case _            => 0
            }
            if (dx != 0 || dy != 0) {
                keyThread = getKeyThread(dx, dy)
                keyThread.start()
            }
            World.currMap.imgPlayerMoving
                .get(lastKey)
                .foreach(World.currMap.player.image = _)
        }
    }
}
