package prog2

import Effects.{Bomb, Heal, Poison, Shield}

class Attack extends Serializable { //extends Copyable[Attack] {

    /** Nom de l'attaque
      */
    var name: String = "Nom"

    /** Quels contrôles afficher pour le lancement de cette attaque.
      * NoSelection: pas de sélection, Direction: une direction, BoardSquare:
      * une case à proximité du monstre
      */
    var selection: AttackSelection = NoSelection

    /** décrit si l'attaque à un effet de protection */
    var shield: Boolean = false

    /** La portée de cette attaque
      */
    var range: Double = 1

    /** La distance max où la case cible peut être sélectionnée, utile
      * uniquement si selection est à BoardSquare
      */
    var selection_range: Double = 1

    /** Le type de l'attaque parmis Normal, Paper, Rock, Cisors
      */
    var attackType: MonsterType.Value = MonsterType.Normal

    /** Coût en PP de l'attaque
      */
    var ppCost: Int = 0

    /** Dégats infligés à l'adversaire
      */
    var hpDamage: Int = 0

    /** Dégats sur les pps infligés à l'adversaire */
    var ppDamage: Int = 0

    /** Description de l'attaque
      */
    var desc: String = ""

    def enemyAffected(b: Board) = !b.curMonster.enemy

    /** Renvoie la liste des positions affectées par l'attaque
      *
      * @param b
      *   le plateau
      * @param sel
      *   la selection effectuée
      * @return
      *   la liste des positions affectées
      */
    def getAffectedPos(b: Board, sel: AttackSelection): List[(Int, Int)] =
        (sel match {
            case NoSelection() => Board.getPosNear(b.curMonsterPos, range)
            case DirectionSelection(dir) => {
                val np = Board.newPos(b.curMonsterPos, dir)
                Range(1, range.toInt).foldLeft(np :: Nil)({ case (l, _) =>
                    Board.newPos(l.head, dir) :: l
                })
            }
            case BoardSquareSelection(pos) => Board.getPosNear(pos, range)
        }).filter(Board.validPos(_))

    /** Effectue l'attaque
      *
      * @param b
      *   le plateau
      * @param sel
      *   la sélection effectuée
      * @return
      *   true si l'attaque affecte au moins un monstre, false sinon
      */
    def perform(b: Board, sel: AttackSelection): Boolean = {
        val enemy_affected = enemyAffected(b)
        val affectedPos = getAffectedPos(b, sel)
        val affectedMonsters = affectedPos
            .flatMap({ case p => b.monsters.get(p) })
            .filter(_.enemy == enemy_affected)
        if (affectedMonsters.isEmpty)
            return false
        affectedMonsters.foreach(_.receiveDamage(this))
        true
    }

    // override def copy(): Attack = new Attack copyFrom this

    // override def copyFrom(o: Attack): Attack = {
    //     name = o.name
    //     selection = o.selection
    //     range = o.range
    //     attackType = o.attackType
    //     ppCost = o.ppCost
    //     hpDamage = o.hpDamage
    //     this
    // }
}
class Heal extends Attack {

    override def enemyAffected(b: Board) = b.curMonster.enemy

    override def perform(b: Board, sel: AttackSelection): Boolean = {
        val ans = super.perform(b, sel)
        val enemy_affected = enemyAffected(b)
        val affectedPos = getAffectedPos(b, sel)
        val affectedMonsters = affectedPos
            .flatMap({ case p => b.monsters.get(p) })
            .filter(_.enemy == enemy_affected)
            .foreach((m: Monster) => if (m.hpByTurn < 0) m.hpByTurn = 0)
        ans
    }
}

class HealObject extends Heal {
    selection = BoardSquareSelection
    selection_range = Int.MaxValue
    range = 0.5
    attackType = MonsterType.Normal
}

class WeakenerObject extends Attack {
    selection = BoardSquareSelection
    selection_range = Int.MaxValue
    range = 0.5
    attackType = MonsterType.Normal
}

class ShieldObject extends Attack {
    override def enemyAffected(b: Board) = b.curMonster.enemy
    selection = BoardSquareSelection
    selection_range = Int.MaxValue
    range = 0.5
    attackType = MonsterType.Normal
    shield = true
}

class StrenghtObject extends Attack {
    override def enemyAffected(b: Board) = b.curMonster.enemy
    selection = BoardSquareSelection
    selection_range = Int.MaxValue
    range = 0.5
    attackType = MonsterType.Normal
}

class AttackObject extends Attack {
    selection = BoardSquareSelection
    selection_range = Int.MaxValue
    range = 0.5
    attackType = MonsterType.Normal
}

object Attack {
    val basic_punch = new Attack {
        name = "Basic punch"
        selection = NoSelection
        range = 1
        attackType = MonsterType.Normal
        ppCost = 5
        hpDamage = 2
    }
    val healing_power = new Heal {
        name = "Heal"
        selection = BoardSquareSelection
        selection_range = 4
        range = 1.5
        attackType = MonsterType.Normal
        ppCost = 20
        hpDamage = -7
        desc = "Heals allies"
    }
    val switch = new Attack {
        name = "Switch"
        selection = BoardSquareSelection
        selection_range = 6
        range = 0.5
        attackType = MonsterType.Normal
        ppCost = 16
        hpDamage = 2
        desc = "Switch positions with\nanother monster"
        override def perform(b: Board, sel: AttackSelection): Boolean = {
            super.perform(b, sel)
            val pos = sel match {
                case BoardSquareSelection(pos) => pos
            }
            b.monsters.remove(b.curMonsterPos)
            b.monsters.get(pos).foreach(b.monsters.put(b.curMonsterPos, _))
            b.monsters.put(pos, b.curMonster)
            b.curMonsterPos = pos
            true
        }
    }
    val weakness = new Attack {
        name = "Weakness"
        selection = BoardSquareSelection
        selection_range = 3.5
        range = 1.5
        attackType = MonsterType.Normal
        ppCost = 18
        ppDamage = 16
        hpDamage = 0
        desc = s"Reduce PP by $ppDamage"
    }
    val shield = new ShieldObject {
        name = "Shield"
        selection = BoardSquareSelection
        selection_range = 5
        range = 1
        ppCost = 25
        hpDamage = 0
        desc = s"Protects allies"
    }
    val ppboost = new StrenghtObject {
        name = "PP Boost"
        selection = DirectionSelection
        range = 4
        attackType = MonsterType.Normal
        ppCost = 16
        ppDamage = -10
        hpDamage = 0
        desc = s"Increase PP by ${-ppDamage}"
    }
    val print = new Attack {
        name = "Print"
        selection = NoSelection
        range = 1
        attackType = MonsterType.Paper
        hpDamage = 2
        ppCost = 12
        desc = s"Print new monsters"
        override def perform(b: Board, sel: AttackSelection): Boolean = {
            super.perform(b, sel)
            val smallSheet = Monster.sheet.copy()
            smallSheet.enemy = b.curMonster.enemy
            smallSheet.ppByTurn = 2
            smallSheet.hp = 3
            smallSheet.pp = 8
            getAffectedPos(b, sel)
                .filterNot(b.monsters.contains(_))
                .foreach(b.monsters.put(_, smallSheet.copy()))
            true
        }
    }
    val wall = new Attack {
        name = "Wall"
        selection = DirectionSelection
        range = 8
        attackType = MonsterType.Rock
        hpDamage = 0
        ppCost = 28
        desc = s"Creates a wall\n-1 PP / turn"
        override def perform(b: Board, sel: AttackSelection): Boolean = {
            super.perform(b, sel)
            b.curMonster.ppByTurn -= 1
            val walls = b.curMonster.copy()
            walls.pp = 0
            walls.hpByTurn = -1
            getAffectedPos(b, sel)
                .filterNot(b.monsters.contains(_))
                .foreach(b.monsters.put(_, walls.copy()))
            true
        }
    }
    val invaders = new Attack {
        name = "Invaders"
        selection = BoardSquareSelection
        range = 2.5
        selection_range = 2.5
        attackType = MonsterType.Scissors
        hpDamage = 1
        ppCost = 25
        desc = s"Spawn invaders\n-1 PP / turn"
        override def perform(b: Board, sel: AttackSelection): Boolean = {
            super.perform(b, sel)
            b.curMonster.ppByTurn -= 1
            if (b.curMonster.ppByTurn <= 0) {
                b.curMonster.ppByTurn = 1
            }
            val inv = b.curMonster.copy()
            inv.hp = 2*(3 + inv.hp) / 3
            inv.hpByTurn = -1
            inv.pp = 12
            val selpos = sel match {
                case BoardSquareSelection(pos) => pos
            }
            getAffectedPos(b, sel)
                .filterNot(b.monsters.contains(_))
                .filterNot(Board.getPosNear(selpos, 1.5).contains(_))
                .foreach(b.monsters.put(_, inv.copy()))
            true
        }
    }

    val rock_punch = new Attack {
        name = "Rock punch"
        selection = NoSelection
        range = 1
        attackType = MonsterType.Rock
        ppCost = 5
        hpDamage = 2
    }
    val rolling_stones = new Attack {
        name = "Rolling stones"
        selection = DirectionSelection
        range = 4
        attackType = MonsterType.Rock
        ppCost = 12
        hpDamage = 5
    }
    val throw_pebble = new Attack {
        name = "Throw pebble"
        selection = DirectionSelection
        range = 3
        attackType = MonsterType.Rock
        ppCost = 7
        hpDamage = 3
    }
    val falling_moais = new Attack {
        name = "Falling moais"
        selection = BoardSquareSelection
        selection_range = 3.5
        range = 1
        attackType = MonsterType.Rock
        ppCost = 13
        hpDamage = 6
    }
    val earthquake = new Attack {
        name = "Earthquake"
        selection = NoSelection
        range = 2.5
        attackType = MonsterType.Rock
        ppCost = 7
        hpDamage = 3
    }
    val sphinx_power = new Attack {
        name = "Sphinx power"
        selection = DirectionSelection
        range = 9
        attackType = MonsterType.Rock
        ppCost = 17
        hpDamage = 9
    }

    val paper_punch = new Attack {
        name = "Paper punch"
        selection = NoSelection
        range = 1
        attackType = MonsterType.Paper
        ppCost = 4
        hpDamage = 1
    }
    val ink_splash = new Attack {
        name = "Ink splash"
        selection = BoardSquareSelection
        selection_range = 2
        range = 1
        attackType = MonsterType.Paper
        ppCost = 7
        hpDamage = 2
    }
    val usps_truck = new Attack {
        name = "USPS truck"
        selection = DirectionSelection
        range = 2
        attackType = MonsterType.Paper
        ppCost = 18
        hpDamage = 5
    }
    val origami = new Attack {
        name = "Origami"
        selection = NoSelection
        range = 1.5
        attackType = MonsterType.Paper
        ppCost = 22
        hpDamage = 7
    }

    val scissors_punch = new Attack {
        name = "Scissors punch"
        selection = NoSelection
        range = 1
        attackType = MonsterType.Scissors
        ppCost = 5
        hpDamage = 2
    }
    val cutcutcut = new Attack {
        name = "Cutcutcut"
        selection = DirectionSelection
        range = 3
        attackType = MonsterType.Scissors
        ppCost = 6
        hpDamage = 2
    }
    val carve = new Attack {
        name = "Carve"
        selection = DirectionSelection
        range = 2
        attackType = MonsterType.Scissors
        ppCost = 7
        hpDamage = 3
    }
    val throw_knife = new Attack {
        name = "Throw knife"
        selection = BoardSquareSelection
        selection_range = 3
        range = 0.5
        attackType = MonsterType.Scissors
        ppCost = 9
        hpDamage = 4
    }
    val stab = new Attack {
        name = "Stab"
        selection = NoSelection
        range = 1
        attackType = MonsterType.Scissors
        ppCost = 11
        hpDamage = 6
    }
    val katana_strike = new Attack {
        name = "Katana strike"
        selection = NoSelection
        range = 1.5
        attackType = MonsterType.Scissors
        ppCost = 17
        hpDamage = 9
    }

    val hole = new Attack {
        name = "Hole"
        selection = BoardSquareSelection
        range = 1.5
        selection_range = 4.5
        attackType = MonsterType.Well
        ppCost = 12
        hpDamage = 5
    }
    val eruption = new Attack {
        name = "Eruption"
        selection = NoSelection
        range = 4
        attackType = MonsterType.Well
        ppCost = 30
        hpDamage = 15
    }
    val lava = new Attack {
        name = "Lava"
        selection = DirectionSelection
        range = 3
        attackType = MonsterType.Well
        ppCost = 9
        hpDamage = 4
    }

    def object_to_attack(obj: InventoryObjects): Attack = {
        val eff = obj.effects
        val power = obj.power
        eff match {
            case Heal =>
                new HealObject {
                    hpDamage = -power
                }
            case Poison =>
                new WeakenerObject {
                    ppDamage = power
                }
            case Bomb =>
                new AttackObject {
                    hpDamage = power
                }
            case Shield => new ShieldObject
        }
    }
}
