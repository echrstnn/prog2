package prog2

import scalafx.geometry.Pos.Center
import scalafx.geometry.{Insets, Pos}
import scalafx.scene.image.{Image, ImageView}
import scalafx.scene.layout.{BorderPane, HBox, StackPane, VBox}
import scalafx.scene.paint.Color.Black
import scalafx.scene.text.Text

object Shop extends Displayable[BorderPane] {

    def buyButton(o:InventoryObjects,price:Int,icon:Image,number:Int,f:Unit=>Unit): StackPane ={
        val base = new ImageView(Graphisms.textefond) {
            fitWidth = 400

            fitHeight = 50
            onMouseClicked = _ => f()
        }
        val name = new Text{
            text = s"${o.name}"
            style = "-fx-font: normal bold 15pt sans-serif"
            fill = Black
        }
        val priceText = new Text{
            text = s": $price"
            style = "-fx-font: normal bold 15pt sans-serif"
            fill = Black
        }
        val numberText = new Text{
            text = s": $number"
            style = "-fx-font: normal bold 15pt sans-serif"
            fill = Black
        }
        val im1 = new ImageView(o.icon()){
            fitWidth = 40
            fitHeight = 40
            margin = Insets(0, 0, 0, 30)
        }
        val im2 = new ImageView(Graphisms.coin){
            fitWidth = 40
            fitHeight = 40
        }
        val im3 = new ImageView(icon){
            fitWidth = 40
            fitHeight = 40
        }


        val contenu = new HBox{
            children = List(im1,name,im2,priceText,im3,numberText)
            mouseTransparent = true
            alignment = Pos.Center
        }

        new StackPane {
            children = List(base,contenu)
            padding = Insets(10, 0, 10, 0)
        }
    }

    def sellerObject(o:InventoryObjects):StackPane=buyButton(o,o.price,Graphisms.backpack,o.num,_=>{if (Player.money>=o.price) {
            Player.moneyUpdate(-o.price)
            o.num+=1
            Shop.display()
        }})


    def upgrades(o:InventoryObjects):StackPane={
        val price = o.price*o.power
        buyButton(o,price,Graphisms.lightening,o.power,_=>{if (Player.money>=price) {
            Player.moneyUpdate(-price)
            o.power+=1
            Shop.display()
        }
        })}


    override protected def displayInit(): BorderPane = {
        val exitButton = ControlButtons.baseButton(
            None,
            "Exit",
            _ => {
                Interface.view = InterfaceView.World
                Interface.scheduleUpdate()
            }
        )
        exitButton.padding = Insets(0, 100, 0, 0)
        new BorderPane {
            alignmentInParent = Pos.Center

            left = new VBox {
                children = List(ControlButtons.baseButton(Some(Graphisms.coin), s"${Player.money}", () => _), exitButton)
                alignment = Pos.Center
            }

            center = new VBox{
                children = ControlButtons.baseButton(None,"Objects",()=>_)::Inventory.inventory.map(sellerObject)
            }


            right = new VBox{
                children = ControlButtons.baseButton(None,"Upgrades",()=>_)::Inventory.inventory.filterNot(o=>{o==Inventory.proteg}).map(upgrades)
            }
            val im = new ImageView(Graphisms.inshop){
            }
            im.alignmentInParent = Pos.Center
            bottom = im
            padding = Insets(10)
        }
    }

    override protected def displayUpdate(displayObj: BorderPane): Unit = {
        val exitButton = ControlButtons.baseButton(
            None,
            "Exit",
            _ => {
                Interface.view = InterfaceView.World
                Interface.scheduleUpdate()
            }
        )
        exitButton.padding = Insets(0, 100, 0, 0)

            displayObj.left = new VBox {
                children = List(ControlButtons.baseButton(Some(Graphisms.coin), s"${Player.money}", () => _), exitButton)
                alignment = Pos.Center
            }

        displayObj.center = new VBox{
                children = ControlButtons.baseButton(None,"Objects",()=>_)::Inventory.inventory.map(sellerObject)
            }


        displayObj.right = new VBox{
                children = ControlButtons.baseButton(None,"Upgrades",()=>_)::Inventory.inventory.filterNot(o=>{o==Inventory.proteg}).map(upgrades)
            }
        }


}
