package prog2

import scalafx.geometry.Insets
import scalafx.geometry.Pos.{Center, CenterLeft}
import scalafx.scene.image._
import scalafx.scene.layout.{Pane, StackPane}
import scalafx.scene.paint.Color.{Black, Gold}
import scalafx.scene.paint.{LinearGradient, Stops}
import scalafx.scene.text.Text

import scala.collection.mutable.ListBuffer
import scalafx.geometry.Pos

object Player {
    var name =""
    var monsters : ListBuffer[Monster] = ListBuffer()
    monsters.appendAll(Monster.allL1)
    var icon = new ImageView(Graphisms.character0)
    var objects : ListBuffer[InventoryObjects] = ListBuffer(Inventory.soins,Inventory.aff,Inventory.atk,Inventory.proteg)
    var money : Int = 0
    var keys : Int =0
    var cheat : Boolean  = true

    var moneyIcon: Text = new Text {
        x =  60
        y = 30
        text = s"${money}"
        style = "-fx-font: normal bold 15pt sans-serif"
        fill = Black
        mouseTransparent = true
    }

    def moneyUpdate(n:Int): Unit ={
        Player.money+=n
        Player.moneyIcon.text = s"${Player.money}"
    }

    var moneyDisplay: StackPane = {

        val buttonBase = new ImageView(Graphisms.attackButtonBase) {
            fitWidth = 90 + 1.86 * moneyIcon.maxWidth(0)
            fitHeight = 50
        }
        new StackPane {
            children = List(
                buttonBase,
                new ImageView(Graphisms.coin) {
                    x = 10
                    y = 5
                    fitWidth = 40
                    fitHeight = 40
                    mouseTransparent = true
                    alignmentInParent = CenterLeft
                },
                moneyIcon
            )
            padding = Insets(10, 0, 10, 0)
            alignmentInParent = Pos.TopCenter
            maxWidth = 70
        }
    }
}