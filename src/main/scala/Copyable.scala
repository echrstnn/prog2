package prog2

/** On fait hériter tout classe T de Copyable[T] si l'on souhaite pouvoir
  * effectuer des copies profondes de T
  */
trait Copyable[T] {

    /** Effectue une copie profonde. À implémenter par (new T).copyFrom(this)
      *
      * @return
      *   une copie profonde de l'objet
      */
    def copy(): T

    /** a.copyFrom(o) effectue une copie profonde de o dans a
      *
      * @param o
      *   l'objet à copier
      * @return
      *   this (pour que copy() soit implémentable en 1 instruction)
      */
    def copyFrom(o: T): T
}
