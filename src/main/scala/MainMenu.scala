package prog2

import scalafx.scene.layout._
import scalafx.scene.image._
import scalafx.scene.text._
import scalafx.geometry._
import scalafx.scene.paint._
import scalafx.scene.control._
import scalafx.application.Platform

object MainMenu extends Displayable[StackPane] {

    var savable = false

    var statusText = new Text {
        text = ""
        style = "-fx-font: normal bold 15pt sans-serif"
        margin = Insets(50, 0, 0, 0)
    }

    var vboxButtons = new VBox {
        alignment = Pos.Center
    }

    def showText(s: String): Unit = {
        statusText.text = s
        Interface.display()
        new Thread {
            override def run(): Unit = {
                Thread.sleep(1000)
                Platform runLater { statusText.text = "" }
            }
        }.start()
    }

    override protected def displayInit(): StackPane = {
        new StackPane {
            children = List(
              new ImageView(Graphisms.moon) { opacity = 0.5 },
              new BorderPane {
                  alignmentInParent = Pos.Center
                  left = new VBox {
                      alignment = Pos.Center
                      children = (0 until WorldGenerator.nbMaps).map((mapId: Int) =>
                          new ImageView(
                            new Image(
                              s"file:src/main/resources/char_${mapId}d.webp.gif"
                            )
                          ) {
                              fitHeight = 100; fitWidth = 100
                              margin = Insets(15)
                          }
                      )
                  }
                  right = new VBox {
                      alignment = Pos.Center
                      children = (0 until WorldGenerator.nbMaps).map((mapId: Int) =>
                          new ImageView(
                            new Image(
                              s"file:src/main/resources/char_${mapId}r.webp.gif"
                            )
                          ) {
                              fitHeight = 100; fitWidth = 100
                              margin = Insets(15)
                          }
                      )
                  }
                  padding = Insets(10)
                  center = vboxButtons

              }
            )
        }
    }

    override protected def displayUpdate(displayObj: StackPane): Unit = {
        vboxButtons.children = List()
        if (savable) {
            vboxButtons.children += ControlButtons.baseButton(
              None,
              "Retour au jeu",
              _ => {
                  Interface.view = InterfaceView.World
                  Interface.display()
              }
            )
        }
        vboxButtons.children += ControlButtons.baseButton(
          None,
          "Nouvelle partie",
          _ => {
              MainMenu.savable = true
              World.initWorld()
              Interface.view = InterfaceView.IntroScreen
              Interface.display()
          }
        )
        val slotsload = new HBox {
            alignment = Pos.Center
            children = new Text {
                text = "Charger depuis slot:"
                style = "-fx-font: normal bold 15pt sans-serif"
                margin = Insets(0, 30, 0, 0)
            } ::
                (for (i <- 1 until (1 + SaveFile.nbSlots))
                    yield ControlButtons.baseButton(
                      None,
                      s"$i",
                      _ => SaveFile.loadFromFile(i)
                    )).toList
        }
        vboxButtons.children += slotsload
        if (savable) {
            val slotssave = new HBox {
                alignment = Pos.Center
                children = new Text {
                    text = "Enregister dans slot:"
                    style = "-fx-font: normal bold 15pt sans-serif"
                    margin = Insets(0, 30, 0, 0)
                } ::
                    (for (i <- 1 until (1 + SaveFile.nbSlots))
                        yield ControlButtons.baseButton(
                          None,
                          s"$i",
                          _ => SaveFile.saveToFile(i)
                        )).toList
            }
            vboxButtons.children += slotssave
        }
        vboxButtons.children += ControlButtons.baseButton(
          None,
          "Combat 1 joueur",
          _ => {
              Interface.monsterSelector = new MonsterSelector() {
                  twoSteps = true
                  availableMonsters = Monster.allMonster
              }
              Interface.view = InterfaceView.MonsterSelection
              Interface.display()
          }
        )
        vboxButtons.children += ControlButtons.baseButton(
          None,
          "Combat 2 joueurs",
          _ => {
              Interface.monsterSelector = new MonsterSelector() {
                  twoSteps = true
                  pvp = true
                  availableMonsters = Monster.allMonster
              }
              Interface.view = InterfaceView.MonsterSelection
              Interface.display()
          }
        )
        vboxButtons.children += ControlButtons.baseButton(
          None,
          "Observer un combat",
          _ => {
              Interface.monsterSelector = new MonsterSelector() {
                  twoSteps = true
                  bvb = true
                  availableMonsters = Monster.allMonster
              }
              Interface.view = InterfaceView.MonsterSelection
              Interface.display()
          }
        )
        vboxButtons.children += ControlButtons.baseButton(
            None,
            "Paramètres de stratégie",
            _ => {
                Interface.view = InterfaceView.StrategySelector
                Interface.display()
            }
        )
        vboxButtons.children += ControlButtons.baseButton(
            None,
            "Son on/off",
            _ => {
                Interface.mediaOn = !Interface.mediaOn
                Interface.changeMusic(0)
            }
        )
        vboxButtons.children += statusText
    }
}
