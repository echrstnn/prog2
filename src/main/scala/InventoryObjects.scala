package prog2

import scalafx.scene.Node
import scalafx.scene.image.Image

import scala.util.Random

class InventoryObjects extends Serializable {

    var effects: Effects.Value = Effects.Bomb

    var name = ""

    var num :Int = 0

    var power : Int = 0

    var price : Int = 10

    def icon (): Image = effects match {
        case Effects.Shield => Graphisms.shield
        case Effects.Poison => Graphisms.iconpoison
        case Effects.Bomb => Graphisms.icondamage
        case Effects.Heal => Graphisms.iconhealth
    }
}

object Inventory {
    val soins: InventoryObjects = new InventoryObjects {
        effects = Effects.Heal
        name ="heal"
        num = 2
        power = 5
    }
    val atk: InventoryObjects = new InventoryObjects {
        effects = Effects.Bomb
        name ="small attack"
        num = 1
        power = 5
    }
    val aff: InventoryObjects = new InventoryObjects {
        effects = Effects.Poison
        name ="poison"
        num = 3
        power = 5
    }
    val proteg: InventoryObjects = new InventoryObjects {
        effects = Effects.Shield
        name ="protection"
        num = 5
    }

    val inventory = List(soins,atk,aff,proteg)

    def award(): Unit = Random
        .shuffle(
            List(
                soins,
                atk,
                aff,
                proteg
            )
        )
        .head.num+=1

    def objectButtons(): List[Node] = {

        Player.objects.filter((i:InventoryObjects)=> i.num!=0).map((o:InventoryObjects) =>
            ControlButtons.baseButton(Some(o.icon),s"${o.name} : ${o.num}",_ => {
                o.num-=1
                ControlButtons.clicked(ControlButtons.actionatk(Attack.object_to_attack(o)))}
            )
        ).toList
    }
}