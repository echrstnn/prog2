package prog2

import scalafx.scene.layout._
import scalafx.scene.image._
import scalafx.scene.text._
import scalafx.geometry._
import scalafx.scene.paint._
import scalafx.scene.control._
import javafx.event.{ActionEvent, EventHandler}
import scalafx.beans.property.ObjectProperty

object StrategySelector extends Displayable[BorderPane] {

    val rdgroup = new ToggleGroup
    val rsgroup = new ToggleGroup
    val ldgroup = new ToggleGroup
    val lsgroup = new ToggleGroup

    def difficultyButton(text: String, time: Int, right: Boolean) =
        new RadioButton(text) {
            padding = Insets(5)
            toggleGroup = if (right) rdgroup else ldgroup
            onAction = _ => if (right) Bot.delaye = time else Bot.delayp = time
            selected = time == (if (right) Bot.delaye else Bot.delayp)
        }

    def strategyButton(
        text: String,
        s: BotStrategy,
        change: Boolean,
        right: Boolean
    ) =
        new RadioButton(text) {
            toggleGroup = if (right) rsgroup else lsgroup
            selected = change
            padding = Insets(5)
            onAction = _ => {
                if (right) {
                    Bot.coefse = s
                    Bot.changee = change
                } else {
                    Bot.coefsp = s
                    Bot.changep = change
                }
            }
        }

    def oneParams(desc: String, right: Boolean) =
        new VBox {
            alignment = Pos.Center
            margin = Insets(100)
            children = List(
              new Text {
                  text = desc
                  style = "-fx-font: normal bold 15pt sans-serif"
              },
              new HBox {
                  margin = Insets(10)
                  children = List(
                    new VBox {
                        margin = Insets(10)
                        children = List(
                          difficultyButton("Novice", 150, right),
                          difficultyButton("Débutant", 350, right),
                          difficultyButton("Confirmé", 1000, right),
                          difficultyButton("Expert", 3000, right),
                          difficultyButton("Maître", 9000, right)
                        )
                    },
                    new VBox {
                        margin = Insets(10)
                        children = List(
                          strategyButton(
                            "Aggressive",
                            BotStrategy.aggressif,
                            false,
                            right
                          ),
                          strategyButton(
                            "Défensive",
                            BotStrategy.defensif,
                            false,
                            right
                          ),
                          strategyButton(
                            "Équilibrée",
                            BotStrategy.mix,
                            false,
                            right
                          ),
                          strategyButton(
                            "Changeante (recommandé)",
                            BotStrategy.pickone(),
                            true,
                            right
                          )
                        )
                    }
                  )
              }
            )
        }

    override protected def displayInit(): BorderPane = {
        val startButton = ControlButtons.baseButton(
          None,
          "Enregister",
          _ => {
              Interface.view = InterfaceView.MainMenu
              Interface.scheduleUpdate()
          }
        )
        val r = oneParams("Adversaire\nOrdi de droite", true)
        new BorderPane {
            alignmentInParent = Pos.Center
            bottom = startButton
            right = oneParams("Adversaire\nOrdi à droite", true)
            left = oneParams("\nOrdi à gauche", false)
            padding = Insets(10)
        }
    }

    override protected def displayUpdate(displayObj: BorderPane): Unit = {}
}
