package prog2

import scalafx.application.Platform
import scalafx.scene.paint.Color.color
import scalafx.scene.shape.Rectangle

object Targets {

    var currAtk = Attack.basic_punch

    def target(range: Double, pos: (Int, Int)): Unit =
        Platform.runLater {
            for ((i, j) <- Board.getPosNear(pos, range))
                Interface.board.gpRange.add(
                  new Rectangle {
                      height = 64
                      width = 64
                      fill = color(0.50, 0.20, 0.20, 0.3)
                      onMouseClicked = _ => {
                          Interface.board.gpRange.children.clear()
                          Interface.board.gpRangeMouse.children.clear()
                          Interface.board.gpRange.mouseTransparent = true
                          new Thread {
                              override def run() = Interface.board.attack(
                                currAtk,
                                BoardSquareSelection(i, j)
                              )
                          }.start()
                      }
                      onMouseEntered = _ => {
                          Interface.board.gpRangeMouse.children.clear()
                          rangemouse(currAtk.range, (i, j))
                      }
                  },
                  i,
                  j
                )
        }

    def rangemouse(range: Double, pos: (Int, Int)) = {
        for ((i, j) <- Board.getPosNear(pos, range)) {
            Interface.board.gpRangeMouse.add(
              new Rectangle {
                  height = 64
                  width = 64
                  fill = color(0.0, 0.20, 0.50, 0.3)
              },
              i,
              j
            )
        }
    }
}
