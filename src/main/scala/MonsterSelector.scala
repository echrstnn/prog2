package prog2

import scalafx.scene.layout._
import scalafx.scene.image._
import scalafx.scene.text._
import scalafx.geometry._
import scalafx.scene.paint._
import scalafx.scene.control._
import scalafx.scene.shape._
import scalafx.util.Duration
import scala.collection.mutable.ListBuffer

class MonsterSelector extends Displayable[BorderPane] {

    var showOnly = false
    var availableMonsters = Player.monsters.toList

    var selectionLimit = 6
    var selectedMonsters: ListBuffer[Monster] = ListBuffer()
    var enemyMonsters: List[Monster] = List()
    var successFun = () => ()
    var twoSteps = false
    var pvp = false
    var bvb = false

    def displayMonsterEntry(m: Monster, c: Color): StackPane = {
        val avail = availableMonsters contains m
        val bgrect = new Rectangle {
            height = 100
            width = 310
            fill = new LinearGradient(
              startX = 0,
              startY = 0,
              endX = 0.5,
              endY = 0.5,
              stops = Stops(c.deriveColor(0, 1, 1, 0.3), Color.BlanchedAlmond)
            )
            alignmentInParent = Pos.Center
        }
        val desc = new HBox {
            alignment = Pos.CenterLeft
            children = List(
              new ImageView(m.img) {
                  margin = Insets(5)
                  fitHeight = 64
                  fitWidth = 64
                  mouseTransparent = true
                  alignmentInParent = Pos.CenterLeft
              },
              new VBox {
                  alignment = Pos.CenterLeft
                  children = List(
                    new Text {
                        text = s"[${m.monsterType}]"
                        mouseTransparent = true
                        style = "-fx-font: normal bold 12pt sans-serif"
                    },
                    new Text {
                        text = s"${m.hp} HP"
                        mouseTransparent = true
                        fill = Color.Red
                        style = "-fx-font: normal bold 11pt sans-serif"
                    },
                    new Text {
                        text = s"${m.ppByTurn} PP / turn"
                        mouseTransparent = true
                        style = "-fx-font: normal 10pt sans-serif"
                    },
                    new Text {
                        text =
                            s"${m.ppStraightMove} or ${m.ppDiagonalMove} PP/move"
                        mouseTransparent = true
                        fill = Color.Grey
                        style = "-fx-font: normal 8pt sans-serif"
                    }
                  )
              }
            )
        }
        val atks = new HBox {
            alignment = Pos.CenterRight
            children = m.attacks.map((atk: Attack) => {
                val c = if (atk.desc == "") Color.Black else Color.Red
                val atkimg = new ImageView(atk.selection match {
                    case NoSelection()           => Graphisms.iconCQC
                    case DirectionSelection(_)   => Graphisms.iconLRA
                    case BoardSquareSelection(_) => Graphisms.iconTA
                }) {
                    fitHeight = 32
                    fitWidth = 32
                    mouseTransparent = true
                }
                val atktype = new Text {
                    text =
                        s"${if (atk.desc == "") atk.attackType.toString.substring(0, 4)
                        else "Spec"}."
                    fill = c
                    mouseTransparent = true
                    style = "-fx-font: normal bold 9pt sans-serif"
                }
                val atkhp = new Text {
                    text = s"${atk.hpDamage.abs} HP"
                    fill = c
                    mouseTransparent = true
                    style = "-fx-font: normal 9pt sans-serif"
                }
                val atkpp = new Text {
                    text = s"${atk.ppCost} PP"
                    mouseTransparent = true
                    style = "-fx-font: normal 8pt sans-serif"
                    fill = Color.Grey
                }
                val atkmore = new Text {
                    text = "\n+ info"
                    style = "-fx-font: normal bold 9pt sans-serif"
                }
                val rangeText =
                    if (atk.selection == BoardSquareSelection)
                        s"${atk.range} + ${atk.selection_range}"
                    else s"${atk.range}"
                Tooltip.install(
                  atkmore,
                  new Tooltip(
                    s"[${atk.attackType}]\n${atk.name}\n\nRange: ${rangeText}\nHP Damage: ${atk.hpDamage}\nCost: ${atk.ppCost}\n\n${atk.desc}"
                  ) {
                      showDelay = Duration(0.1)
                      hideDelay = Duration(0.1)
                      font = Font(14)
                  }
                )
                new VBox {
                    padding = Insets(0, 6, 0, 6)
                    children = List(atkimg, atktype, atkhp, atkpp, atkmore)
                    alignment = Pos.Center
                }
            })
        }
        new StackPane {
            alignmentInParent = Pos.Center
            margin = Insets(5)
            maxWidth = 310
            children = List(bgrect, desc, atks)
            opacity =
                if (avail || c == Color.Red || c == Color.Green) 1 else 0.5
            onMouseClicked = if (avail && !showOnly) { _ =>
                {
                    if (c != Color.Red) {
                        if (
                          selectedMonsters.length < selectionLimit && !selectedMonsters
                              .contains(m)
                        )
                            selectedMonsters += m
                        else
                            selectedMonsters -= m
                        Interface.scheduleUpdate()
                    }
                }
            } else { _ => {} }
        }

    }

    override protected def displayInit(): BorderPane = {
        val startButton =
            ControlButtons.baseButton(
              None,
              "Commencer",
              _ => {
                  if (!selectedMonsters.isEmpty) {
                      if (enemyMonsters.isEmpty && twoSteps) {
                          enemyMonsters = selectedMonsters.toList
                          selectedMonsters = ListBuffer()
                          Interface.display()
                      } else {
                          selectedMonsters = selectedMonsters.map(_.copy())
                          enemyMonsters = enemyMonsters.map(_.copy())
                          selectedMonsters.foreach(_.enemy = false)
                          enemyMonsters.foreach(_.enemy = true)
                          val candidatesPos = Board.boardPos.zipWithIndex
                              .filter(_._2 % 3 == 0)
                              .map(_._1)
                          val candidatesPos1 = candidatesPos
                          //   candidatesPos.slice(0,3) :::
                          //   candidatesPos.slice(6, 9)
                          val candidatesPos2 = candidatesPos.reverse
                          //   .slice(0, 3) ::: candidatesPos.reverse.slice(6, 9)
                          Interface.board.monsters.clear()
                          candidatesPos1
                              .zip(selectedMonsters)
                              .foreach({ case (p, m) =>
                                  Interface.board.monsters.put(p, m)
                              })
                          candidatesPos2
                              .zip(enemyMonsters)
                              .foreach({ case (p, m) =>
                                  Interface.board.monsters.put(p, m)
                              })
                          Interface.board.monsters.values
                              .foreach(_.pp =
                                  12 - Interface.board.monsters.size
                              )
                          Interface.board.pvp = pvp
                          Interface.board.bvb = bvb
                          Bot.sidePenalitye = 0;
                          Bot.sidePenalityp = 0;
                          Interface.view = InterfaceView.Fight
                          Interface.board.newTurn()
                          Interface.scheduleUpdate()
                          if (!pvp)
                              new Thread {
                                  override def run(): Unit = {
                                      var ans = true
                                      while (
                                        (bvb || Interface.board.curMonster.enemy) && ans && Interface.view == InterfaceView.Fight
                                      ) {
                                          val move =
                                              Bot.botChoice(Interface.board)
                                          ans = move(Interface.board)
                                          if (!ans)
                                              println("wrong choice")
                                      }
                                  }
                              }.start()
                      }
                  }
              }
            )
        val exitButton = ControlButtons.baseButton(
          None,
          "Cancel",
          _ => {
              Interface.view = Interface.nextView
              Interface.scheduleUpdate()
          }
        )
        exitButton.padding = Insets(0, 100, 0, 0)
        new BorderPane {
            alignmentInParent = Pos.Center
            bottom = new HBox {
                children =
                    if (showOnly) List(exitButton)
                    else List(exitButton, startButton)
                alignment = Pos.Center
            }
            if (!showOnly) {
                left = new ScrollPane
                right = new ScrollPane
            }
            center = new ScrollPane
            padding = Insets(10)
        }
    }

    override protected def displayUpdate(displayObj: BorderPane): Unit = {
        displayObj.center = new ScrollPane {
            alignmentInParent = Pos.Center
            vvalue = displayObj
                .center()
                .asInstanceOf[javafx.scene.control.ScrollPane]
                .getVvalue
            prefWidth = 340
            maxWidth = 340
            content = new VBox {
                alignment = Pos.Center
                children = (availableMonsters ::: Monster.allMonster.filterNot(
                  availableMonsters.contains(_)
                )).map((m: Monster) =>
                    displayMonsterEntry(
                      m,
                      if (selectedMonsters.contains(m)) Color.Green
                      else Color.Beige
                    )
                )
            }
        }
        if (!showOnly) {
            displayObj.left = new ScrollPane {
                alignmentInParent = Pos.Center
                vvalue = displayObj
                    .left()
                    .asInstanceOf[javafx.scene.control.ScrollPane]
                    .getVvalue
                prefWidth = 340
                content = new VBox {
                    alignment = Pos.Center
                    children = selectedMonsters.map(
                      displayMonsterEntry(_, Color.Green)
                    )
                }
            }
            displayObj.right = new ScrollPane {
                alignmentInParent = Pos.Center
                vvalue = displayObj
                    .right()
                    .asInstanceOf[javafx.scene.control.ScrollPane]
                    .getVvalue
                prefWidth = 340
                content = new VBox {
                    alignment = Pos.Center
                    children =
                        enemyMonsters.map(displayMonsterEntry(_, Color.Red))
                }
            }
            displayObj.top = new Text {
                text =
                    s"${selectedMonsters.length} / $selectionLimit monsters selected"
                mouseTransparent = true
                style = "-fx-font: normal 12pt sans-serif"
            }
        }
    }
}
