package prog2

/** On fait hériter une classe de Displayable[T] si l'on souhaite pouvoir
  * l'afficher à l'aide d'un objet de type T
  */
trait Displayable[T] {

    /** Indique si l'objet a déjà été affiché ou non
      */
    private var displayed = false

    /** L'objet de scalaFX qui sert à afficher la classe fille
      */
    var displayObj: T = _

    /** Initialise l'affichage de la classe
      *
      * @return
      *   l'objet de scalaFX qui servira à afficher la classe
      */
    protected def displayInit(): T

    /** Met à jour l'objet scalaFX pour mettre à jour l'interface
      *
      * @param displayObj
      *   l'objet de scalaFX qui sert à afficher la classe
      */
    protected def displayUpdate(displayObj: T): Unit = {}

    /** Construit l'objet de scalaFX qui correspond à l'affichage de la classe.
      * Si l'objet n'a jamais été affiché, appelle displayInit puis
      * displayUpdate, sinon appelle uniquement displayUpdate
      *
      * @return
      *   l'objet scalaFX correpondant à l'affichage de la classe
      */
    final def display(): T = {
        if (!displayed)
            displayObj = displayInit()
        displayUpdate(displayObj)
        displayed = true
        displayObj
    }
}
