package prog2

import prog2.Dialogues.funNext
import scalafx.geometry.Insets
import scalafx.geometry.Pos.Center
import scalafx.scene.image._
import scalafx.scene.layout.{Pane, StackPane}
import scalafx.scene.text.Text

class Dialogues {
    var lines: List[String] = List()

    var x: Iterator[String] = lines.toIterator

    def next(f: () => Boolean): Unit = {
        if (x.hasNext)
            Dialogues.updateText(x.next())
        else {
            f()
            x = lines.toIterator
            Dialogues.funNext = () => ()
            Dialogues.updateText(Dialogues.savedText)
            Dialogues.displayed = false
        }
    }

    def displayDialogue(f: () => Boolean): Boolean = {
        if (!Dialogues.displayed) {
            Dialogues.displayed = true
            funNext = () => next(f)
            funNext()
        }
        true

    }
}

object Dialogues {

    def fromList(l: List[String]): Dialogues = {
        new Dialogues {
            lines = l
            x = lines.toIterator
        }
    }

    var savedText = ""

    var displayed = false

    def updateText(string: String): Unit = {
        World.currMap.dialogueText.text = string
    }

    var funNext: () => Unit = () => ()

    def textObj() =
        new Text {
            x = 38
            y = 30
            text = ""
            style = "-fx-font: normal bold 10pt sans-serif"
            mouseTransparent = true
        }

    def baseText(s: String, texto: Text): Pane = {
        savedText = s
        texto.text = s
        val textBase = new ImageView(Graphisms.textefond) {
            fitWidth = 70 + 1.4 * texto.maxWidth(0)

            fitHeight = 50 + 1.4 * texto.maxHeight(0)
            onMouseClicked = _ => funNext()
        }
        new StackPane {
            children = List(textBase, texto)
            padding = Insets(10, 0, 10, 0)
            alignmentInParent = Center
        }
    }

    def refresh(): Unit = {
        updateText(savedText)
        displayed = false
    }
}
