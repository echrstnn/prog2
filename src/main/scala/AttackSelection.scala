package prog2

sealed trait AttackSelection

case class NoSelection() extends AttackSelection
case class DirectionSelection(dir: BoardDir.Value) extends AttackSelection
case class BoardSquareSelection(pos: (Int, Int)) extends AttackSelection

object NoSelection extends NoSelection()
object DirectionSelection extends DirectionSelection(BoardDir.N)
object BoardSquareSelection extends BoardSquareSelection((0, 0))
