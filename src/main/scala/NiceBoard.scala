package prog2

import scalafx.scene._
import scalafx.scene.image.ImageView
import scalafx.scene.shape._
import scalafx.scene.layout._
import scalafx.scene.paint.Color._
import scalafx.application._
import scalafx.geometry.Pos._
import scalafx.geometry._

import scala.util.Random

class NiceBoard(_b: Board) extends Board with Displayable[Group] {

    this copyFrom _b

    private def delay(): Unit =
        Thread.sleep(if(bvb) 350 else 500)

    private def newGridPane = new GridPane {
        mouseTransparent = true
        columnConstraints =
            for (_ <- 0 until Board.size) yield new ColumnConstraints(74) {
                halignment = HPos.Center
            }
        rowConstraints =
            for (_ <- 0 until Board.size) yield new RowConstraints(74) {
                valignment = VPos.Center
            }
    }

    val gpBackground = newGridPane
    var gpCracks = newGridPane
    val gpMonsters = newGridPane
    val gpOverlay = newGridPane
    var gpRange = newGridPane
    var gpRangeMouse = newGridPane

    override def move(dir: BoardDir.Value): Boolean = {
        println(s"MOV $dir")
        if (bvb || curMonster.enemy && !pvp) {
            Platform.runLater {
                gpOverlay.add(
                  new ImageView(Graphisms.dirToIm(dir, false)) {
                      fitHeight = 24;
                      fitWidth = 24
                      alignmentInParent = BottomRight
                  },
                  curMonsterPos._1,
                  curMonsterPos._2
                )
            }
            delay()
        }
        val ans = super.move(dir)
        Platform.runLater {
            gpOverlay.children.clear
        }
        Interface.scheduleUpdate()
        if (bvb || curMonster.enemy && !pvp)
            delay()
        ans
    }

    override def attack(attack: Attack, sel: AttackSelection): Boolean = {
        println(s"ATK ${attack.name}  $sel")
        if (bvb || curMonster.enemy && !pvp) {
            Platform.runLater {
                sel match {
                    case NoSelection() =>
                        gpOverlay.add(
                          new ImageView(Graphisms.iconCQC) {
                              fitHeight = 32
                              fitWidth = 32
                              alignmentInParent = BottomRight
                          },
                          curMonsterPos._1,
                          curMonsterPos._2
                        )
                    case DirectionSelection(dir) =>
                        gpOverlay.add(
                          new ImageView(Graphisms.iconLRA) {
                              fitHeight = 32;
                              fitWidth = 32
                              alignmentInParent = BottomRight
                          },
                          curMonsterPos._1,
                          curMonsterPos._2
                        )
                    case BoardSquareSelection(pos) =>
                        gpOverlay.add(
                          new ImageView(Graphisms.iconTA) {
                              fitHeight = 32;
                              fitWidth = 32
                              alignmentInParent = BottomRight
                          },
                          curMonsterPos._1,
                          curMonsterPos._2
                        )
                        gpOverlay.add(
                          new ImageView(Graphisms.interro) {
                              fitHeight = 74;
                              fitWidth = 74
                              alignmentInParent = Center
                          },
                          pos._1,
                          pos._2
                        )
                }
            }
            delay()
        }
        val affectedPos = attack.getAffectedPos(this, sel)
        val ans = super.attack(attack, sel)

        val cracks = sel match {
            case DirectionSelection(BoardDir.SE) =>
                affectedPos
                    .flatMap(
                      { case (a, b) =>
                          List(
                            (None, (a, b)),
                            (Some(BoardDir.NE), (a - 1, b)),
                            (Some(BoardDir.SW), (a, b - 1))
                          )
                      }
                    )
                    .filter(x => Board.validPos(x._2))
            case DirectionSelection(BoardDir.SW) =>
                affectedPos
                    .flatMap(
                      { case (a, b) =>
                          List(
                            (None, (a, b)),
                            (Some(BoardDir.NW), (a + 1, b)),
                            (Some(BoardDir.SE), (a, b - 1))
                          )
                      }
                    )
                    .filter(x => Board.validPos(x._2))
            case DirectionSelection(BoardDir.NE) =>
                affectedPos
                    .flatMap(
                      { case (a, b) =>
                          List(
                            (None, (a, b)),
                            (Some(BoardDir.SE), (a - 1, b)),
                            (Some(BoardDir.NW), (a, b + 1))
                          )
                      }
                    )
                    .filter(x => Board.validPos(x._2))
            case DirectionSelection(BoardDir.NW) =>
                affectedPos
                    .flatMap(
                      { case (a, b) =>
                          List(
                            (None, (a, b)),
                            (Some(BoardDir.SW), (a + 1, b)),
                            (Some(BoardDir.NE), (a, b + 1))
                          )
                      }
                    )
                    .filter(x => Board.validPos(x._2))
            case DirectionSelection(_) => affectedPos.map(p => (None, p))
            case NoSelection() =>
                Board
                    .getPosNear(curMonsterPos, attack.range + 1)
                    .map(
                      { case (c, d) =>
                          (
                            curMonsterPos._1 compare c,
                            curMonsterPos._2 compare d
                          ) match {
                              case (-1, -1) => (Some(BoardDir.NW), (c, d))
                              case (-1, 1)  => (Some(BoardDir.SW), (c, d))
                              case (1, -1)  => (Some(BoardDir.NE), (c, d))
                              case (1, 1)   => (Some(BoardDir.SE), (c, d))
                              case _        => (None, (c, d))
                          }
                      }
                    )
                    .filter(x =>
                        Board.validPos(x._2) && (!x._1.isEmpty)
                    ) ::: affectedPos.map(p => (None, p))
            case BoardSquareSelection(posSel) =>
                Board
                    .getPosNear(posSel, attack.range + 1)
                    .map(
                      { case (c, d) =>
                          (posSel._1 compare c, posSel._2 compare d) match {
                              case (-1, -1) => (Some(BoardDir.NW), (c, d))
                              case (-1, 1)  => (Some(BoardDir.SW), (c, d))
                              case (1, -1)  => (Some(BoardDir.NE), (c, d))
                              case (1, 1)   => (Some(BoardDir.SE), (c, d))
                              case _        => (None, (c, d))
                          }
                      }
                    )
                    .filter(x =>
                        Board.validPos(x._2) && (x._1 match {
                            case None => false
                            case Some(BoardDir.NW) =>
                                affectedPos.contains(
                                  (x._2._1 - 1, x._2._2)
                                ) && affectedPos.contains(
                                  (x._2._1, x._2._2 - 1)
                                ) && (!affectedPos.contains(x._2))
                            case Some(BoardDir.SW) =>
                                affectedPos.contains(
                                  (x._2._1 - 1, x._2._2)
                                ) && affectedPos
                                    .contains(
                                      (x._2._1, x._2._2 + 1)
                                    ) && (!affectedPos.contains(x._2))
                            case Some(BoardDir.NE) =>
                                affectedPos.contains(
                                  (x._2._1 + 1, x._2._2)
                                ) && affectedPos
                                    .contains(
                                      (x._2._1, x._2._2 - 1)
                                    ) && (!affectedPos.contains(x._2))
                            case Some(BoardDir.SE) =>
                                affectedPos.contains(
                                  (x._2._1 + 1, x._2._2)
                                ) && affectedPos
                                    .contains(
                                      (x._2._1, x._2._2 + 1)
                                    ) && (!affectedPos.contains(x._2))
                            case _ => false
                        })
                    ) ::: affectedPos.map(p => (None, p))
        }
        Platform.runLater {
            gpOverlay.children.clear
            cracks.foreach(pos =>
                gpCracks.add(
                  pos._1 match {
                      case Some(BoardDir.SE) =>
                          new ImageView(Graphisms.crackSE) {
                              fitWidth = 74
                              fitHeight = 74
                          }
                      case Some(BoardDir.NE) =>
                          new ImageView(Graphisms.crackNE) {
                              fitWidth = 74
                              fitHeight = 74
                          }
                      case Some(BoardDir.SW) =>
                          new ImageView(Graphisms.crackSW) {
                              fitWidth = 74
                              fitHeight = 74
                          }
                      case Some(BoardDir.NW) =>
                          new ImageView(Graphisms.crackNW) {
                              fitWidth = 74
                              fitHeight = 74
                          }
                      case _ =>
                          attack match {
                              case _: Heal =>
                                  new ImageView(Graphisms.healFond) {
                                      if (attack.range > 1) {
                                          fitWidth = 32
                                          fitHeight = 32
                                          x = 16
                                          y = 16
                                      } else {
                                          fitWidth = 74
                                          fitHeight = 74

                                      }
                                  }
                              case _: WeakenerObject =>
                                  new ImageView(Graphisms.poisonFond) {
                                      fitWidth = 74
                                      fitHeight = 74
                                  }
                              case _: ShieldObject =>
                                  new ImageView(Graphisms.shieldFond) {
                                      fitWidth = 74
                                      fitHeight = 74
                                  }
                              case _ =>
                                  new ImageView(
                                    Random
                                        .shuffle(
                                          List(
                                            Graphisms.crack1,
                                            Graphisms.crack2,
                                            Graphisms.crack3
                                          )
                                        )
                                        .head
                                  ) {
                                      fitWidth = 74
                                      fitHeight = 74
                                  }
                          }
                  },
                  pos._2._1,
                  pos._2._2
                )
            )

        }
        delay()
        Interface.scheduleUpdate()
        Platform.runLater {
            gpCracks.children.clear
        }
        delay()
        ans
    }

    override def newTurn(): Boolean = {
        if (bvb || curMonster.enemy && !pvp)
            delay()
        val ans = super.newTurn()
        println(s"NEW now $curMonsterPos")
        Interface.controls.inputBlocked = bvb || Interface.board.curMonster.enemy && !pvp
        if (monsters.forall(_._2.enemy))
            Interface.view = Interface.nextView
        if (monsters.forall(!_._2.enemy)) {
            Interface.view = Interface.nextView
            Interface.monsterSelector.successFun()
        }
        
        Interface.scheduleUpdate()
        ans
    }

    override protected def displayInit(): Group = {
        for ((x, y) <- Board.boardPos)
            gpBackground.add(
              new ImageView(Graphisms.sol1) {
                  fitHeight = 73; fitWidth = 73
              },
              x,
              y
            )
        // gpRange.mouseTransparent = false
        gpMonsters.mouseTransparent = false
        new Group(
          gpBackground,
          gpCracks,
          gpMonsters,
          gpOverlay,
          gpRange,
          gpRangeMouse
        ) {
            alignmentInParent = Pos.Center
        }
    }

    override protected def displayUpdate(displayObj: Group): Unit = {
        gpMonsters.children.clear()
        for (((x, y), m) <- monsters)
            gpMonsters.add(m.display(), x, y)

        gpMonsters.add(
          new Circle {
              radius = 6
              fill = Red
              alignmentInParent = TopRight
          },
          curMonsterPos._1,
          curMonsterPos._2
        )
    }
}
