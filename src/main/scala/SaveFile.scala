package prog2

import java.io.FileInputStream
import java.io.ObjectInputStream
import java.io.FileOutputStream
import java.io.ObjectOutputStream
import java.io.FileNotFoundException
import scala.collection.mutable._

object SaveFile {
    val nbSlots = 4
    var saveFile = new SaveFile

    def registerCall(mapId: Int, f: WorldMap => Unit) =
        saveFile.calls += ((mapId, f))
    def unregisterCall(mapId: Int, f: WorldMap => Unit) =
        saveFile.calls -= ((mapId, f))

    def loadFromFile(saveId: Int): Unit = {
        try {
            val file = new FileInputStream(s"savefile-$saveId.dat")
            val objin = new ObjectInputStream(file)
            objin.readObject().asInstanceOf[SaveFile].loadVars()
            objin.close()
            MainMenu.savable = true
            MainMenu.showText(s"Sauvegarde $saveId chargée")
        } catch {
            case e: FileNotFoundException => {
                MainMenu.showText(
                  s"Il n'y a pas de sauvegarde dans le slot $saveId"
                )
            }
        }
    }

    def saveToFile(saveId: Int) = {
        saveFile.saveVars()
        val file = new FileOutputStream(s"savefile-$saveId.dat", false)
        val objout = new ObjectOutputStream(file)
        objout.writeObject(saveFile)
        objout.close()
        MainMenu.showText(s"Sauvegardé dans slot $saveId")
    }
}

class SaveFile extends Serializable {
    var calls: ListBuffer[(Int, WorldMap => Unit)] = ListBuffer()

    var name = ""
    var monsters: List[Monster] = List()
    var objects: List[InventoryObjects] = List()
    var money: Int = 0
    var cheat: Boolean = true
    var mapId = 0
    var playerX, playerY = 0.0
    var focusX, focusY = 0.0

    def loadVars() {
        SaveFile.saveFile = new SaveFile
        World.maps = (for (i <- 0 until WorldGenerator.nbMaps)
            yield new WorldMap(i)).toArray
        calls.reverse.foreach({ case (i, f) => f(World.maps(i)) })
        World.currMap = World.maps(mapId)
        World.currMap.player.layoutX = playerX
        World.currMap.player.layoutY = playerY
        World.currMap.scrollPane.hvalue = focusX
        World.currMap.scrollPane.vvalue = focusY
        Player.name = name
        Player.monsters = ListBuffer() ++ monsters
        Player.objects = ListBuffer() ++ objects
        Player.money = money
        Player.cheat = cheat
    }

    def saveVars() {
        name = Player.name
        monsters = Player.monsters.toList
        objects = Player.objects.toList
        money = Player.money
        cheat = Player.cheat
        mapId = World.maps.indexOf(World.currMap)
        playerX = World.currMap.player.layoutX()
        playerY = World.currMap.player.layoutY()
        focusX = World.currMap.scrollPane.hvalue()
        focusY = World.currMap.scrollPane.vvalue()
    }
}
