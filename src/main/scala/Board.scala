package prog2

import scalafx.scene._
import scalafx.scene.image.ImageView
import scalafx.scene.shape._
import scalafx.scene.paint._
import scalafx.scene.layout._
import scalafx.scene.paint.Color.Transparent
import scala.util.Random
import scala.collection.mutable.TreeMap
import scalafx.application.Platform
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListMap

object Board {

    /** Taille du côté du plateau
      */
    val size = 8

    val boardPos: List[(Int, Int)] =
        Range(0, size).flatMap((i: Int) => Range(0, size).map((i, _))).toList

    /** Teste si une position est valide
      *
      * @param pos
      *   la position à tester
      * @return
      *   true si la position est sur le plateau, false sinon
      */
    def validPos(pos: (Int, Int)) =
        0 <= pos._1 && pos._1 < size && 0 <= pos._2 && pos._2 < size

    /** Calcule la distance euclidienne entre deux positions
      *
      * @param a
      *   une position
      * @param b
      *   une position
      * @return
      *   la distance euclidienne en nombre de cases
      */
    def distance(a: (Int, Int), b: (Int, Int)): Double =
        Math.sqrt((a._1 - b._1) * (a._1 - b._1) + (a._2 - b._2) * (a._2 - b._2))

    /** Calcule la liste des positions aux alentours d'une position
      *
      * @param pos
      *   la position dont on veut calculer le voisinage
      * @param distance
      *   le rayon du voisinage
      * @return
      *   la liste des positions à une distance inférieure à distance de pos
      */
    def getPosNear(pos: (Int, Int), distance: Double): List[(Int, Int)] =
        boardPos.filter(Board.distance(pos, _) <= distance)

    /** Calcule la position après un déplacement
      *
      * @param pos
      *   la position initiale
      * @param dir
      *   la direction du déplacement
      * @return
      *   la position après un déplacement dans la direction dir depuis pos
      */
    def newPos(pos: (Int, Int), dir: BoardDir.Value): (Int, Int) = {
        val (dx, dy) = BoardDir.toPos(dir)
        (pos._1 + dx, pos._2 + dy)
    }
}

class Board extends Copyable[Board] {

    /** Map qui associe à chaque position (valide) le monstre qui se trouve
      * dessus
      */
    var monsters = HashMap[(Int, Int), Monster]()

    /** Le monstre qui a la main sur ce tour
      */
    var curMonster: Monster = new Monster

    /** La position du monstre qui a la main sur ce tour
      */
    var curMonsterPos: (Int, Int) = (0, 0)

    var pvp = false
    var bvb = false

    /** Termine le tour en cours et lance un nouveau tour
      *
      * Done la main au monstre différent de curMonster qui possède le plus de
      * PP et appelle newTurn pour chaque monstre. Le monstre qui reprend la
      * main ne gagne pas de PP lors du changement de tour.
      *
      * @return
      *   false si le jeu est terminé, true sinon
      */
    def newTurn(): Boolean = {
        monsters = monsters.filter({ case ((i, j), m) =>
            if (m.hp < 1) false
            else true
        })
        if (
          monsters.values.filter(_.enemy).isEmpty || monsters.values
              .filterNot(_.enemy)
              .isEmpty
        )
            return true
        val newCurMonster =
            monsters
                .filter(_._1 != curMonsterPos)
                .maxBy({ case ((i, j), m) => (m.pp, m.hp, j, i) })
        curMonster = newCurMonster._2
        curMonsterPos = newCurMonster._1
        val pp = curMonster.ppByTurn
        curMonster.ppByTurn = 0
        monsters.values.foreach(_.newTurn())
        curMonster.ppByTurn = pp
        curMonster.underShield = false
        true
    }

    /** Déplace curMonster selon une direction donnée si c'est possble et met à
      * jour curMonsterPos et la map monsters
      *
      * @param dir
      *   la direction vers laquelle tenter le déplacement
      * @return
      *   true si curMonster a bien été déplacé, false si le déplacement a
      *   échoué (sortie du plateau, case déjà occupée, PP insuffisants) auquel
      *   cas curMonster n'est pas déplacé
      */
    def move(dir: BoardDir.Value): Boolean = {
        val newPos = Board.newPos(curMonsterPos, dir)
        if (!Board.validPos(newPos) || monsters.contains(newPos))
            return false
        if (!curMonster.move(dir))
            return false
        monsters.put(newPos, curMonster)
        monsters.remove(curMonsterPos)
        curMonsterPos = newPos
        true
    }

    /** Fait attaquer curMonster
      *
      * @param attack
      *   l'attaque à effectuer
      * @param sel
      *   la sélection choisie
      * @return
      *   true si l'attaque réussi et touche un monstre, false sinon
      */
    def attack(attack: Attack, sel: AttackSelection): Boolean = {
        if (!curMonster.attack(attack, sel))
            return false
        val b = attack.perform(this, sel)
        monsters = monsters.filter({ case ((i, j), m) =>
            if (m.hp < 1) false
            else true
        });
        b
    }

    override def equals(o2: Any): Boolean = {
        val o = o2.asInstanceOf[Board]
        o.curMonsterPos == curMonsterPos && monsters.forall({
            case ((i, j), m) =>
                o.monsters.getOrElse((i, j), new Monster).equals(m)
        }) && o.monsters.forall({ case ((i, j), m) =>
            monsters.getOrElse((i, j), new Monster).equals(m)
        })
    }

    override def hashCode(): Int = {
        var ans = curMonster.hashCode()
        ans = (ans*11 + curMonsterPos._1 ) *11 + curMonsterPos._2
        ans = ans * 1000003 + monsters
            .map({ case ((i, j), m) =>
                (m.hashCode() * 11 + i) * 11 + j
            })
            .sum
        ans
    }

    override def copy(): Board = new Board copyFrom this

    override def copyFrom(o: Board): Board = {
        monsters = o.monsters.map({ case ((x, y), m) =>
            ((x, y), m.copy())
        })
        curMonsterPos = o.curMonsterPos.copy()
        curMonster = monsters.get(curMonsterPos).get
        pvp = o.pvp
        bvb = o.bvb
        this
    }

}
