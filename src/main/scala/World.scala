package prog2
import javafx.scene
import scalafx.Includes._
import scalafx.scene.layout._
import scalafx.scene.image._
import scalafx.scene.text._
import scalafx.geometry._
import scalafx.scene.control._
import scalafx.application._
import scalafx.scene.input._
import scalafx.scene.control.ScrollPane._
import javafx.geometry

import scala.collection.mutable
import scala.util.Random

object World extends Displayable[BorderPane] {

    var maps: Array[WorldMap] =
        (for (i <- 0 until WorldGenerator.nbMaps) yield new WorldMap(i)).toArray

    var currMap = maps(0)

    def initWorld(): Unit = {
        maps = (for (i <- 0 until WorldGenerator.nbMaps)
            yield new WorldMap(i)).toArray
        maps(0).addShop()
        WorldGenerator.generate()
        Monster.allL1.foreach(maps(0).addMonster(_, 2, 0, Nil))
        Monster.allL2.foreach(maps(0).addMonster(_, 2, 1, Monster.allL1))
        Monster.allL2.foreach(
          maps(1).addMonster(_, 3, 2, Monster.allL1 ::: Monster.allL2)
        )
        Monster.allL3.foreach(
          maps(1).addMonster(_, 4, 3, Monster.allL1 ::: Monster.allL2)
        )
        Monster.allL3.foreach(
          maps(2).addMonster(_, 5, 4, Monster.allL2 ::: Monster.allL3)
        )
        Monster.allL4.foreach(
          maps(2).addMonster(_, 6, 5, Monster.allL2 ::: Monster.allL3)
        )
        maps(3).addMonster(
          Monster.milo,
          4,
          2,
          List(Monster.milo, Monster.katana)
        )
        maps(3).addMonster(
          Monster.crane,
          3,
          5,
          List(
            Monster.milo,
            Monster.couteau,
            Monster.menhir,
            Monster.menhir,
            Monster.menhir
          )
        )
        maps(3).addMonster(
          Monster.couteau,
          2,
          2,
          List(Monster.milo, Monster.swan)
        )
        maps(3).addMonster(
          Monster.volcan,
          6,
          5,
          Monster.puits :: Monster.allL3 ::: Monster.allL4
        )
        maps(4).addMonster(Monster.puits, 6, 5, Monster.allL2 ::: Monster.allL3)
        Monster.allL4.foreach(
          maps(4).addMonster(_, 6, 5, Monster.allL3 ::: Monster.allL4)
        )
        maps.foreach((wm: WorldMap) => {
            for (_ <- 0 until 3) { wm.addChest(ChestsAward.chest1) }
            for (_ <- 0 until 1) { wm.addChest(ChestsAward.chest2) }
        })
        currMap = maps(0)
    }

    override protected def displayInit(): BorderPane = {
        new BorderPane
    }

    override protected def displayUpdate(displayObj: BorderPane): Unit = {
        displayObj.center = currMap.display()
    }
}
