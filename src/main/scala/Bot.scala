package prog2

import scala.util.Random
import scala.collection.mutable.HashMap
import scala.collection.mutable.WeakHashMap
import scalafx.scene.text.FontWeight

class BotStrategy {
    var bPpByTurn, bHp, bPp, bShield: Int = 0
    var pPpByTurn, pHp, pPp, pShield: Int = 0
}

object BotStrategy {
    val aggressif = new BotStrategy {
        bPpByTurn = 300
        bHp = 25
        bPp = 1
        bShield = 50
        pPpByTurn = 500
        pHp = 30
        pPp = 2
        pShield = 60
    }
    val defensif = new BotStrategy {
        bPpByTurn = 500
        bHp = 30
        bPp = 1
        bShield = 120
        pPpByTurn = 300
        pHp = 25
        pPp = 2
        pShield = 15
    }
    val mix = new BotStrategy {
        bPpByTurn = 400
        bHp = 25
        bPp = 2
        bShield = 75
        pPpByTurn = 400
        pHp = 25
        pPp = 2
        pShield = 75
    }

    def pickone(): BotStrategy = {
        Random.shuffle(List(aggressif, defensif, mix)).head
    }
}

object Bot {

    case class RecCall(b: Board, depth: Int)

    def isSideCol(i: Int, startRight: Boolean) =
        (startRight && (i == Board.size - 1 || i == Board.size - 2)) ||
            (!startRight && (i == 0 || i == 1))
    var sidePenality = 0
    var sidePenalityp, sidePenalitye = 0

    val INF = 1000 * 1000 * 1000
    var botSide = true
    var coefs: BotStrategy = BotStrategy.pickone()
    var coefsp, coefse = BotStrategy.pickone()
    var delayp, delaye = 1000
    var changep, changee = true
    var cache: HashMap[RecCall, (Double, Board => Boolean)] = HashMap()

    /** Calcule l'ensemble de coups que l'ordinateur souhaite jouer
      *
      * @param b
      *   le plateau
      * @return
      *   une fonction qui une fois apellée sur un plateau effectue les coups
      *   choisis dessus
      */
    def botChoice(b: Board): Board => Boolean = {
        cache.clear()
        botSide = b.curMonster.enemy
        if (
          b.monsters
              .filter({ case ((i, j), m) => m.enemy == botSide })
              .keys
              .forall({ case (i, j) => isSideCol(i, botSide) })
        ) {
            if (b.curMonster.enemy) sidePenalitye += 1
            else sidePenalityp += 1
        } else {
            if (b.curMonster.enemy) sidePenalitye = -2
            else sidePenalityp = -2
        }
        sidePenality = if (b.curMonster.enemy) sidePenalitye else sidePenalityp
        println(s"side penality is $sidePenality")
        if (
          ((botSide && changee) || (!botSide && changep)) &&
          Random.nextInt(5) == 0
        ) {
            println("CHANGE STRATEGY")
            if (b.curMonster.enemy) coefse = BotStrategy.pickone()
            else coefsp = BotStrategy.pickone()
        }
        coefs = if (b.curMonster.enemy) coefse else coefsp
        if (b.monsters.values.forall(_.enemy == botSide))
            return (b: Board) => b.newTurn()
        var depth = 2
        var ans = (b: Board) => b.newTurn()
        val searchThread = new Thread {
            override def run(): Unit = {
                while (depth <= 20) {
                    ans = score(depth, b, Double.MinValue, Double.MaxValue)._2
                    depth += 1
                }
            }
        }
        searchThread.start()
        Thread.sleep(if (botSide) delaye else delayp)
        searchThread.stop()
        println(s"depth $depth")
        ans
    }

    /** Heuristique qui évalue le score du plateau
      *
      * Un plateau favorable au joueur a un score élevé, un plateau favorable à
      * l'ordinateur a un score faible
      *
      * @param b
      *   le plateau
      * @return
      *   le score du plateau
      */
    private def scoreHeuristic(b: Board): Double = {
        if (b.monsters.values.forall(_.enemy == botSide))
            return -INF
        if (b.monsters.values.forall(_.enemy != botSide))
            return +INF
        val player = b.monsters.values.filter(_.enemy != botSide)
        val enemy = b.monsters.filter({ case ((i, j), m) =>
            m.enemy == botSide
        })
        val ps = player
            .map({ case m =>
                coefs.pPpByTurn * m.ppByTurn +
                    coefs.pHp * m.hp +
                    coefs.pPp * m.pp +
                    (if (m.underShield) coefs.pShield else 0)
            })
            .sum
        val es = enemy
            .map({ case ((i, j), m) =>
                coefs.bPpByTurn * m.ppByTurn +
                    coefs.bHp * m.hp +
                    coefs.bPp * m.pp +
                    (if (m.underShield) coefs.bShield else 0) -
                    (if (b.bvb && isSideCol(i, m.enemy) && sidePenality > 0)
                         sidePenality
                     else 0)
            })
            .sum
        (ps - es)
    }

    private val steps =
        ((b: Board) => b.newTurn()) ::
            (for (dir <- BoardDir.values.toList)
                yield (b: Board) => b.move(dir)) :::
            Nil

    /** Calcule les coups possibles sur un plateau
      *
      * @param b
      *   le plateau
      * @return
      *   un couple de liste. La première liste représente les plateau qu'on
      *   peut obtenir après avoir effectué le coup correspondant dans la
      *   seconde liste
      */
    def possibilities(
        b: Board
    ): (List[Board], List[Board => Boolean]) = {

        if (
          b.monsters.values
              .forall(_.enemy) || b.monsters.values.forall(!_.enemy)
        )
            return (Nil, Nil)

        val attacks = b.curMonster.attacks.flatMap({ case a =>
            a.selection match {
                case NoSelection() =>
                    // if (
                    //   a.getAffectedPos(b, NoSelection())
                    //       .exists(b.monsters.contains(_))
                    // )
                        ((b: Board) => b.attack(a, NoSelection())) :: Nil
                    // else Nil
                case DirectionSelection(_) =>
                    for (
                      dir <- BoardDir.values.toList
                      if a
                          .getAffectedPos(b, DirectionSelection(dir))
                          .exists(b.monsters.contains(_))
                    )
                        yield (b: Board) => b.attack(a, DirectionSelection(dir))
                case BoardSquareSelection(_) =>
                    for (
                      pos <- Board
                          .getPosNear(b.curMonsterPos, a.selection_range)
                      //   if a.getAffectedPos(b, BoardSquareSelection(pos))
                      //   .exists(b.monsters.contains(_))
                    )
                        yield (b: Board) =>
                            b.attack(a, BoardSquareSelection(pos))
            }
        })
        val moves = attacks ::: steps

        var ans = (for (move <- moves)
            yield {
                val newb = b.copy()
                val valid = move(newb)
                (newb, move, valid)
            }).filter(_._3).unzip3
        // println(s"${ans.length} possibilities")
        (ans._1, ans._2)
    }

    /** Fonction principale de l'algorithme alpha-beta
      *
      * @param depth
      *   la profondeur max à explorer dans ce sous arbre
      * @param b
      *   le plateau racine du sous arbre
      * @param _alpha
      *   le alpha de l'algorithme alpha-beta
      * @param _beta
      *   le beta de l'algorithme alpha-beta
      * @return
      *   un couple: le score associé au sommet correspondant à b et une
      *   fonction qui prend en paramètre le plateau et effectue les coups
      *   décidés par l'algorithme
      */
    private def score_(
        depth: Int,
        b: Board,
        _alpha: Double,
        _beta: Double
    ): (Double, Board => Boolean) = {
        var (alpha, beta) = (_alpha, _beta)

        var bestMove = //(b: Board) => true
            if (b.curMonster.enemy == botSide) (b: Board) => b.newTurn()
            else (b: Board) => true
        if (depth == 0)
            return (scoreHeuristic(b), bestMove)

        val (possibleBoards, possibleMoves) = possibilities(b)
        if (possibleBoards.isEmpty)
            return (scoreHeuristic(b), bestMove)
        var it = (possibleBoards zip possibleMoves)

        var ans: Double = 0
        if (b.curMonster.enemy == botSide) {
            ans = Double.MaxValue
            for ((ib, imove) <- it) {
                var (recAns, recMove) = score(depth - 1, ib, alpha, beta)
                if (recAns < ans) {
                    ans = recAns
                    bestMove =
                        ((b: Board) => if (imove(b)) recMove(b) else false)
                }
                beta = Math.min(beta, ans)
                if (ans <= alpha)
                    return (ans, bestMove)
            }
        } else {
            ans = Double.MinValue
            for ((ib, imove) <- it) {
                var (recAns, recMove) = score(depth - 1, ib, alpha, beta)
                if (recAns > ans)
                    ans = recAns
                alpha = Math.max(alpha, ans)
                if (beta <= ans)
                    return (ans, bestMove)
            }

        }
        (ans, bestMove)
    }

    private def score(
        depth: Int,
        b: Board,
        _alpha: Double,
        _beta: Double
    ): (Double, Board => Boolean) = {
        cache.getOrElseUpdate(
          RecCall(b, depth),
          score_(depth, b, _alpha, _beta)
        )
    }

}
