package prog2

import scalafx.scene.image._
import scala.util.Random

object Graphisms {
    val imN = new Image("file:src/main/resources/N.png")
    val imS = new Image("file:src/main/resources/S.png")
    val imE = new Image("file:src/main/resources/E.png")
    val imW = new Image("file:src/main/resources/W.png")
    val imNE = new Image("file:src/main/resources/NE.png")
    val imNW = new Image("file:src/main/resources/NW.png")
    val imSE = new Image("file:src/main/resources/SE.png")
    val imSW = new Image("file:src/main/resources/SW.png")
    val imNa = new Image("file:src/main/resources/Na.png")
    val imSa = new Image("file:src/main/resources/Sa.png")
    val imEa = new Image("file:src/main/resources/Ea.png")
    val imWa = new Image("file:src/main/resources/Wa.png")
    val imNEa = new Image("file:src/main/resources/NEa.png")
    val imNWa = new Image("file:src/main/resources/NWa.png")
    val imSEa = new Image("file:src/main/resources/SEa.png")
    val imSWa = new Image("file:src/main/resources/SWa.png")
    val sol1 = new Image("file:src/main/resources/sol1.png")
    val crack1 = new Image("file:src/main/resources/crack1.png")
    val crack2 = new Image("file:src/main/resources/crack2.png")
    val crack3 = new Image("file:src/main/resources/crack3.png")
    val crackNE = new Image("file:src/main/resources/crackNE.png")
    val crackNW = new Image("file:src/main/resources/crackNW.png")
    val crackSE = new Image("file:src/main/resources/crackSE.png")
    val crackSW = new Image("file:src/main/resources/crackSW.png")
    val iconCQC = new Image("file:src/main/resources/iconCQC.png")
    val iconLRA = new Image("file:src/main/resources/iconLongRangeAttack.png")
    val iconTA = new Image("file:src/main/resources/iconTrowingAttack.png")
    val attackButtonBase = new Image(
      "file:src/main/resources/attackButtonBase.png"
    )
    val interro = new Image("file:src/main/resources/interro.png")
    val rip = new Image("file:src/main/resources/rip.png")
    val menhir = new Image("file:src/main/resources/menhir.png")
    val moai = new Image("file:src/main/resources/moai.png")
    val milo = new Image("file:src/main/resources/milo.png")
    val sphinx = new Image("file:src/main/resources/sphinx.png")
    val sheet = new Image("file:src/main/resources/sheet.png")
    val letter = new Image("file:src/main/resources/letter.png")
    val swan = new Image("file:src/main/resources/swan.png")
    val crane = new Image("file:src/main/resources/crane.png")
    val ciseaux = new Image("file:src/main/resources/ciseaux.png")
    val ciseau = new Image("file:src/main/resources/ciseau.png")
    val couteau = new Image("file:src/main/resources/couteau.png")
    val katana = new Image("file:src/main/resources/katana.png")
    val puits = new Image("file:src/main/resources/puit.png")
    val volcan = new Image("file:src/main/resources/volcan.png")
    val map = new Image("file:src/main/resources/map.png")
    val character0 = new Image("file:src/main/resources/char_0d.webp.png")
    val shield = new Image("file:src/main/resources/shield.png")
    val shieldFond = new Image("file:src/main/resources/shieldFond.png")
    val poisonFond = new Image("file:src/main/resources/poisonFond.png")
    val healFond = new Image("file:src/main/resources/healFond.png")
    val nbuissons = Array(4, 2, 1, 4, 1)
    val buisson_ = (for (i <- 0 until WorldGenerator.nbMaps)
        yield (for (j <- 0 until nbuissons(i))
            yield new Image(
              s"file:src/main/resources/buisson${i}_$j.png"
            )).toList).toArray
    val nterres = Array(32, 4, 1, 50, 7)
    val terre_ = (for (i <- 0 until WorldGenerator.nbMaps)
        yield (for (j <- 0 until nterres(i))
            yield new Image(
              s"file:src/main/resources/terre${i}_$j.png"
            )).toList).toArray
    val icondamage = new Image("file:src/main/resources/icondamage.png")
    val iconhealth = new Image("file:src/main/resources/iconhealth.png")
    val iconpoison = new Image("file:src/main/resources/iconpoison.png")
    val textefond = new Image("file:src/main/resources/textefond.png")
    val coffre1 = new Image("file:src/main/resources/coffre1.png")
    val coffre2 = new Image("file:src/main/resources/coffre2.png")
    val coin = new Image("file:src/main/resources/coin.png")
    val shop = new Image("file:src/main/resources/shop.png")
    val backpack = new Image("file:src/main/resources/backpack.png")
    val lightening = new Image("file:src/main/resources/lightening.png")
    val inshop = new Image("file:src/main/resources/inshop.png")
    val teleporter_ = new Image("file:src/main/resources/teleporter.png")
    val printer = new Image("file:src/main/resources/printer.png")
    val wall = new Image("file:src/main/resources/wall.png")
    val invader = new Image("file:src/main/resources/invader.png")
    val moon = new Image(s"file:src/main/resources/moon.png")

    def dirToIm(dir: BoardDir.Value, atk: Boolean) = dir match {
        case BoardDir.N  => if (atk) imNa else imN
        case BoardDir.S  => if (atk) imSa else imS
        case BoardDir.E  => if (atk) imEa else imE
        case BoardDir.W  => if (atk) imWa else imW
        case BoardDir.NE => if (atk) imNEa else imNE
        case BoardDir.NW => if (atk) imNWa else imNW
        case BoardDir.SE => if (atk) imSEa else imSE
        case BoardDir.SW => if (atk) imSWa else imSW
    }

    def teleporter(mapId: Int) = teleporter_

    def terre(mapId: Int) = Random
        .shuffle(terre_(mapId))
        .head
    def buisson(mapId: Int): Image = {
        val l = buisson_(mapId)
        Random
            .shuffle(
              l.head :: l.head :: l
            )
            .head
    }

}
