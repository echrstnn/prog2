package prog2

import scala.util.Random
import java.io._
import scalafx.scene.image.Image

object Chests extends Enumeration with Serializable {
    val Rare, Courant = Value
}

class ChestsAward extends Serializable {
    var chestType: Chests.Value = Chests.Rare

    var icon = Graphisms.coffre1

    private def writeObject(oos: ObjectOutputStream) = {
        oos.writeObject(chestType)
        oos.writeObject(icon.getUrl())
    }

    private def readObject(ois: ObjectInputStream) = {
        chestType = ois.readObject().asInstanceOf[Chests.Value]
        icon = new Image(ois.readObject().asInstanceOf[String])
    }

    def award(): Unit = {
        chestType match {
            case Chests.Rare =>
                Inventory.award()
                Inventory.award()
                Player.moneyUpdate(30 + Random.nextInt(30))
            case Chests.Courant =>
                Inventory.award()
                Player.moneyUpdate(10 + Random.nextInt(10))
        }
    }
}

object ChestsAward {
    val chest1 = new ChestsAward {
        chestType = Chests.Courant
        icon = Graphisms.coffre2
    }
    val chest2 = new ChestsAward
}
