package prog2

import scalafx.application._

object Main extends JFXApp3 {

    override def start(): Unit = {
        stage = new JFXApp3.PrimaryStage {
            width = 1100
            height = 700
            scene = Interface.display()
        }
        stage.setMinHeight(700)
        stage.setMinWidth(1100)
    }
}
